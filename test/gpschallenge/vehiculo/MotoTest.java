package gpschallenge.vehiculo;

import org.junit.Test;

public class MotoTest {

	@Test
	public void testCambioDeVehiculo() {
		IVehiculo unVehiculo = new Moto(); 

		org.junit.Assert.assertSame((new Auto()).getClass(), unVehiculo.vehiculoDeCambio().getClass());
	}

}
