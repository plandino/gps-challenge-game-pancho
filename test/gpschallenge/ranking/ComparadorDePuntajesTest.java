package gpschallenge.ranking;

import static org.junit.Assert.*;

import org.junit.Test;

public class ComparadorDePuntajesTest {

	@Test
	public void testVerificarQueUnPuntajeEsMenorQueElOtroCorrectamente() {
		Puntaje menor = new Puntaje("Tobi",1);
		Puntaje mayor = new Puntaje("Juan",9);
		ComparadorDePuntajes unComparador = new ComparadorDePuntajes();
		assertEquals((unComparador.compare(menor, mayor)),1);
	}
	
	@Test
	public void testVerificarQueUnPuntajeEsMayorQueElOtroCorrectamente() {
		Puntaje mayor = new Puntaje("Tobi",8);
		Puntaje menor = new Puntaje("Juan",3);
		ComparadorDePuntajes unComparador = new ComparadorDePuntajes();
		assertEquals((unComparador.compare(mayor, menor)),-1);
	}
	
	@Test
	public void testVerificarQueUnPuntajeEsIgualQueElOtroCorrectamente() {
		// Cuando dos puntajes son iguales devuelve -1
		Puntaje igual1 = new Puntaje("Tobi",8);
		Puntaje igual2 = new Puntaje("Juan",8);
		ComparadorDePuntajes unComparador = new ComparadorDePuntajes();
		assertEquals((unComparador.compare(igual1, igual2)),-1);
	}

}
