package gpschallenge.ranking;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.csvreader.CsvWriter;

public class RankingTest {

	private String rutaDelRanking = "test/gpschallenge/ranking/rankingPruebas.csv";
	
	@Before
	public final void setUp(){
		// Esto se encarga de crear el archivo, para que cuando cree un Raking, el archivo a leer exista
		crearArchivosCSVParaRankingTest();
	}
	
	@Test
	public void testSeSeteoCorrectamenteElNombre() {
		Ranking unRanking = new Ranking(rutaDelRanking);
		unRanking.colocarEnElRanking(new Puntaje("Mari",5));
		ArrayList<Puntaje> unArrayDePuntajes = unRanking.getListaPuntajes();
		
        assertEquals((unArrayDePuntajes.get(0).getNombre()),"Mari");	
	}
	
	@Test
	public void testSeSeteoCorrectamenteElPuntaje() {
		Ranking unRanking = new Ranking(rutaDelRanking);
		unRanking.colocarEnElRanking(new Puntaje("Fede",9));
		ArrayList<Puntaje> unArrayDePuntajes = unRanking.getListaPuntajes();
		
        assertEquals((unArrayDePuntajes.get(0).getPuntaje()), 9 , 0.1);	
	}
	
	@Test
	public void testSeSetearonCorrectamenteLosPuntajesEnOrdenDescendente() {
		Ranking unRanking = new Ranking(rutaDelRanking);
		unRanking.colocarEnElRanking(new Puntaje("Mari",5));
		unRanking.colocarEnElRanking(new Puntaje("Joaquin",6));
		unRanking.colocarEnElRanking(new Puntaje("Fede",2));
		ArrayList<Puntaje> unArrayDePuntajes = unRanking.getListaPuntajes();
		
        assertEquals((unArrayDePuntajes.get(0).getNombre()),"Joaquin");
        assertEquals((unArrayDePuntajes.get(0).getPuntaje()), 6 , 0.1);
        assertEquals((unArrayDePuntajes.get(1).getNombre()),"Mari");
        assertEquals((unArrayDePuntajes.get(1).getPuntaje()), 5 , 0.1);
        assertEquals((unArrayDePuntajes.get(2).getNombre()),"Fede");
        assertEquals((unArrayDePuntajes.get(2).getPuntaje()), 2 , 0.1);
	}
	
	@Test
	public void testNoDeberianHaberMasDeDiezJugadoresEnElRanking() {
		int tamanioMaximoDelRanking = 10;
		Ranking unRanking = new Ranking(rutaDelRanking);
		// La cantidad maxima de puntajes en el ranking es 10, pero yo le introduzco 15
		for(int i = 0; i<15 ; ++i){
			unRanking.colocarEnElRanking(new Puntaje("Juan",i));
		}
		ArrayList<Puntaje> unArrayDePuntajes = unRanking.getListaPuntajes();
		assertEquals((unArrayDePuntajes.size()),tamanioMaximoDelRanking);		
	}
	
	@Test
	public void testSiNoExisteElRankingDevulveUnArrayListVacio(){
		String rutaInvalida = "test/gpschallenge/ranking/nombreInvalido.csv";
		Ranking unRanking = new Ranking(rutaInvalida);
		ArrayList<Puntaje> unArrayDePuntajes = unRanking.getListaPuntajes();
		assertEquals(true, unArrayDePuntajes.isEmpty());
		// Cuando trato de leer de una ruta invalida, se crea el archivo y devulve un ArrayList<Puntaje>, por eso, luego tengo que borrar el archivo sin usar
		borrarArchivoSinUsar(rutaInvalida);
		
	}
	
	// Este es automatico, borra el archivo que se crea de pruebas siempre
	@After 
	public void tearDown(){
		File fileABorrar = new File(rutaDelRanking);
		fileABorrar.delete();
	}
	
	public void crearArchivosCSVParaRankingTest(){
		CsvWriter escritorCsv = null;
		try {
            // Crea el escritor para archivos CSV. Al poner false por parametro, sobreescribe el archivo. Tambien le introduce las cabeceras
            escritorCsv = new CsvWriter(new FileWriter(rutaDelRanking, false), ',');
           	escritorCsv.write("Nombre");
           	escritorCsv.write("Puntaje");
           	escritorCsv.endRecord(); 
            escritorCsv.close();
         
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        	escritorCsv.close();
        }
	}
	
	// Este se usa para borrar el archivo que se crea debido a que se introdujo una ruta invalida
	public void borrarArchivoSinUsar(String ruta){
		File fileABorrar = new File(ruta);
		fileABorrar.delete();
	}
}
