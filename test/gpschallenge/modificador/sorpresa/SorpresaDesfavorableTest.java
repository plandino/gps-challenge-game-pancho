package gpschallenge.modificador.sorpresa;

import static org.junit.Assert.*;
import gpschallenge.Jugador;
import gpschallenge.Partida;
import gpschallenge.mapa.Mapa;
import gpschallenge.modificador.Modificador;

import org.junit.Test;

public class SorpresaDesfavorableTest {
	private final double DELTA = 1e-15;
	
	@Test 
	public void testTodosDebenRestar20PorCiento() {
		Partida partida = new Partida(new Mapa(), new Jugador());
    	Sorpresa sorpresa = new SorpresaDesfavorable();
    	Modificador obtenido = sorpresa.obtenerModificador(partida);
    	assertEquals(obtenido.productoDeMovimientos, 0.25, DELTA);
	}
}
