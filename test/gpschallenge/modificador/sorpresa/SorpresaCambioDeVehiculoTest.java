package gpschallenge.modificador.sorpresa;

import static org.junit.Assert.*;
import gpschallenge.Jugador;
import gpschallenge.Partida;
import gpschallenge.mapa.Mapa;
import gpschallenge.modificador.Modificador;

import org.junit.Test;


public class SorpresaCambioDeVehiculoTest {
	@Test
	public void testDebeMarcarCambioDeVehiculo() {
		Partida partida = new Partida(new Mapa(), new Jugador());
		Sorpresa sorpresa = new SorpresaCambioDeVehiculo();
		Modificador obtenido = sorpresa.obtenerModificador(partida);
		assertEquals(obtenido.cambioDeVehiculo, true);
	}
}
 