package gpschallenge.modificador.obstaculo;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import gpschallenge.mapa.Mapa;
import gpschallenge.modificador.*;
import gpschallenge.vehiculo.*;
import gpschallenge.Jugador;
import gpschallenge.Partida;

public class PozoTest {
	
	private Partida partida;
	@Before
	public void setUp() {
		this.partida = new Partida(new Mapa(), new Jugador());
	}
    @Test 
    public void testAutoYMotoDebenSerPenalizadosCon3() {
    	ArrayList<IVehiculo> vehiculos = new ArrayList<IVehiculo>();
    	vehiculos.add(new Auto());
    	vehiculos.add(new Moto());
    	for(IVehiculo vehiculo : vehiculos) {
    		Obstaculo pozo = new Pozo();
    		
    		partida.getJugador().setVehiculo((Vehiculo) vehiculo);
    		Modificador obtenido = pozo.obtenerModificador(partida);
    		Modificador esperado = new Modificador();
    		esperado.adicionDeMovimientos = 3;
            //esperado.
    		assertEquals(obtenido, esperado);
    		
    	}
    }
    
    @Test 
    public void testCamioneta4x4NoDebeSerPenalizada() {
		Obstaculo pozo = new Pozo();
		partida.getJugador().setVehiculo((Vehiculo) new Camioneta4x4());
		Modificador obtenido = pozo.obtenerModificador(partida);
		Modificador esperado = new Modificador();
        //esperado.
		assertEquals(obtenido, esperado);   	
    }

/* Pozos: Le suma 3 movimientos de penalización a autos y motos, pero no afecta a las 4x4. */
/* Piquete: Autos y 4x4 deben pegar la vuelta, no pueden pasar. Las motos puede pasar con una penalización de 2 movimientos. */
/* Control Policial: Para todos los vehículos la penalización es de 3 movimientos, sin embargo la probabilidad de que el vehículo quede demorado por el control y sea penalizado es de 0,3 para las 4x4, 0,5 para los autos y 0,8 para las motos ya que nunca llevan el casco puesto. */
}
