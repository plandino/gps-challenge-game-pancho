package gpschallenge.modificador.obstaculo;

import static org.junit.Assert.*;

import java.util.ArrayList;

import gpschallenge.Jugador;
import gpschallenge.Partida;
import gpschallenge.mapa.Mapa;
import gpschallenge.modificador.Modificador;
import gpschallenge.vehiculo.Auto;
import gpschallenge.vehiculo.Camioneta4x4;
import gpschallenge.vehiculo.IVehiculo;
import gpschallenge.vehiculo.Moto;
import gpschallenge.vehiculo.Vehiculo;

import org.junit.Before;
import org.junit.Test;

// TODO : Como no puedo probar el random, probablemente pueda probar el coeficiente de probabilidad 
// en algun lado.  Probablemente una funcion protegida.  Analizar eso y hacer el cambio luego.
public class ControlPolicialTest {
	private final double DELTA = 1e-15;
	private Partida partida;

	@Before
	public void setUp() {
		this.partida = new Partida(new Mapa(), new Jugador());
	}
    @Test 
    public void testCualquierVehiculoDebeSerPenalizadoCon3MovimientosONoSerPenalizadoEnAbsoluto() {
    	ArrayList<IVehiculo> vehiculos = new ArrayList<IVehiculo>();
    	vehiculos.add(new Auto());
    	vehiculos.add(new Moto());
    	vehiculos.add(new Camioneta4x4());
    	for(IVehiculo vehiculo : vehiculos) {
    		Obstaculo control = new ControlPolicial();
    		partida.getJugador().setVehiculo((Vehiculo) vehiculo);
    		Modificador obtenido = control.obtenerModificador(partida);
    		assert(obtenido.adicionDeMovimientos == 3 || obtenido.adicionDeMovimientos == 0);
    	}
    }
    @Test 
    public void testProbabilidadDeSerDemoradoCamioneta4x4() {
    	double prob = (new Camioneta4x4()).getProbabilidadDeSerDemoradoPorControl();
       	assertEquals(prob, 0.3, DELTA);      	
    }
    @Test 
    public void testProbabilidadDeSerDemoradoAuto() {
    	double prob = (new Auto()).getProbabilidadDeSerDemoradoPorControl();
       	assertEquals(prob, 0.5, DELTA);      	
    }
    @Test 
    public void testProbabilidadDeSerDemoradoMoto() {
    	double prob = (new Moto()).getProbabilidadDeSerDemoradoPorControl();
    	assertEquals(prob, 0.8, DELTA);
    }  
    
}
