package gpschallenge.modificador.obstaculo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import gpschallenge.mapa.Mapa;
import gpschallenge.modificador.*;
import gpschallenge.vehiculo.*;
import gpschallenge.Jugador;
import gpschallenge.Partida;

public class PiqueteTest {
	private Partida partida;
	@Before
	public void setUp() {
		this.partida = new Partida(new Mapa(), new Jugador());
	}
    @Test
    public void testConstructor() {
        /* Jugador jug = new Jugador(); */
        @SuppressWarnings("unused")
		Auto automovil = new Auto();
        @SuppressWarnings("unused")
		Obstaculo obs = new Piquete();
    }

    @Test
    public void testAutoDebePegarLaVuelta() {
        /* Piquete: Autos y 4x4 deben pegar la vuelta, no pueden pasar. Las motos puede pasar con una penalización de 2 movimientos. */
		Obstaculo piquete = new Piquete();
		partida.getJugador().setVehiculo((Vehiculo) new Auto());
		Modificador obtenido = piquete.obtenerModificador(partida);
		Modificador esperado = new Modificador();
        esperado.movimientoExitoso = false;
        assertEquals(obtenido, esperado);
    }
    @Test
    public void testCamioneta4x$DebePegarLaVuelta() {
        /* Piquete: Autos y 4x4 deben pegar la vuelta, no pueden pasar. Las motos puede pasar con una penalización de 2 movimientos. */
		Obstaculo piquete = new Piquete();
		partida.getJugador().setVehiculo((Vehiculo) new Camioneta4x4());
        Modificador obtenido = piquete.obtenerModificador(partida);
        Modificador esperado = new Modificador();
        esperado.movimientoExitoso = false;
        assertEquals(obtenido, esperado);
    }

    @Test
    public void testMotoSiguePeroPenaliza2Movs() {
		Obstaculo piquete = new Piquete();
		partida.getJugador().setVehiculo((Vehiculo) new Moto());
        Modificador obtenido = piquete.obtenerModificador(partida);
        Modificador esperado = new Modificador();
        esperado.movimientoExitoso = true;
        esperado.adicionDeMovimientos = 2;
        assertEquals(obtenido, esperado);
        
    }
}
