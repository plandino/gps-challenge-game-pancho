package gpschallenge.modificador;

import static org.junit.Assert.*;

import org.junit.Test;

public class ModificadorTest {

	@Test
	public void testEqualsConNullEsFalso() {
		Modificador mod = new Modificador();
		assertFalse(mod.equals(null));
	}
	
	@Test 
	public void testEqualsConOtroObjetoEsFalso() {
		Modificador mod = new Modificador();
		assertFalse(mod.equals(new Integer(2)));
	}
	
	@Test
	public void testAdicionDeMovimientosComparacion() {
		Modificador mod = new Modificador();
		Modificador mod2 = new Modificador();
		mod.adicionDeMovimientos = 2;
		mod2.adicionDeMovimientos = 3;
		assertFalse(mod.equals(mod2));
		
		mod2.adicionDeMovimientos = 2;
		assertTrue(mod.equals(mod2));
		assertEquals(mod, mod2);
	}
}
