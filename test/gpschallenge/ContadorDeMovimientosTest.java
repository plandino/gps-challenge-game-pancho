package gpschallenge;

import static org.junit.Assert.*;
import gpschallenge.ContadorDeMovimientos;

import org.junit.Before;
import org.junit.Test;

public class ContadorDeMovimientosTest {

	private double inicial;
	private ContadorDeMovimientos contador;
	private final double DELTA = 1e-15;
	
	@Before
	public void setUp() {
		inicial = 5.0;
		contador = new ContadorDeMovimientos(inicial);
	}
	@Test
	public void testSumaBienLaCantidadDeMovimientos() {
		ContadorDeMovimientos unContadorDeMov = new ContadorDeMovimientos(0.0);
		float cantidadDeMovimientosASumar = 5;
		unContadorDeMov.sumarCantidadDeMovimientos(cantidadDeMovimientosASumar);
		//En el assertEquals, cuando comparas dos floats, se pone un delta para poder compararlos, ya que en el redondeo de la maquina, se puede generar un error
		assertEquals((unContadorDeMov.getCantidadDeMovimientos()) , cantidadDeMovimientosASumar , 0.1);
	}
	
	@Test public void testInicializaLaCantidadDeMovimientosEnCero(){
		ContadorDeMovimientos unContadorDeMov = new ContadorDeMovimientos(0.0);
		//En el assertEquals, cuando comparas dos floats, se pone un delta para poder compararlos, ya que en el redondeo de la maquina, se puede generar un error
		assertEquals((unContadorDeMov.getCantidadDeMovimientos()) , 0 , 0.1);
	}
	
	@Test 
	public void testSumar() {
		contador.sumarCantidadDeMovimientos(2.0);
		assertEquals(7.0, contador.getCantidadDeMovimientos(), DELTA);
	}
	public void testSumarNegativo() {
		contador.sumarCantidadDeMovimientos(-2.0);
		assertEquals(3.0, contador.getCantidadDeMovimientos(), DELTA);
	}
	
	@Test 
	public void testMultiplicar() {
		contador.productoCoef(0.25);
		assertEquals(1.25*5, contador.getCantidadDeMovimientos(), DELTA);
	}
	
	@Test 
	public void testSetACero() {
		contador.resetACero();
		assertEquals(0, contador.getCantidadDeMovimientos(), DELTA);
	}
	
	@Test 
	public void testLimiteDeMovimientos100PorDefecto() {
		assertEquals(100.0, contador.getLimiteDeMovimientos(), DELTA);
	}
	
	@Test 
	public void testLimiteDeMovimientoGetSet() {
		double expected = 30.0;
		contador.setLimiteDeMovimientos(expected);
		assertEquals(30.0, contador.getLimiteDeMovimientos(), DELTA);
	}
	
	@Test
	public void testCalculaBienLaCantidadDeMovimientosSobrantes(){
		int limiteMovimientos = 20;
		contador.setLimiteDeMovimientos(limiteMovimientos);
		double cantidadDeMovimientosEsperados = limiteMovimientos - inicial;
		assertEquals(cantidadDeMovimientosEsperados, contador.getCantidadDeMovimientosSobrantes(), 0.1);
	}
	
	
	
}
