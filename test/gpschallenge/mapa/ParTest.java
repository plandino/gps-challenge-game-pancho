package gpschallenge.mapa;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Test;

public class ParTest {
	@Test
	public void testDestinoDeUnElemento() {
		Cruce unCruce = new Cruce(0);
		Cruce otroCruce = new Cruce(1);
		Par<Cruce> par = new Par<Cruce>(unCruce, otroCruce);
		assertEquals(par.getOpuesto(unCruce), otroCruce);
	}
	@Test(expected = IllegalArgumentException.class)
	public void testDeberiaLanzarExcepcionAlCrearUnParConDosCrucesIguales() {
		Cruce unCruce = new Cruce(1);
		Cruce otroCruce = new Cruce(1);
		@SuppressWarnings("unused")
		Par<Cruce> par = new Par<Cruce>(unCruce, otroCruce);
	}
	
	@Test 
	public void testContieneA() {
		Cruce cruce1 = new Cruce(1);
		Cruce cruce2 = new Cruce(3);
		Cruce cruce3 = new Cruce(5);
		Par<Cruce> par = new Par<Cruce>(cruce1, cruce2);
		assertTrue(par.contieneA(cruce1));
		assertTrue(par.contieneA(new Cruce(1)));
		assertTrue(par.contieneA(cruce2));
		assertFalse(par.contieneA(cruce3));
	}
	
	@Test 
	public void testDosElementosEqualsEnOrdenDebenDarParesIguales() {
		Integer uno = new Integer(1);
		Integer dos = new Integer(2);
		Par<Integer> parInt1 = new Par<Integer>(uno, dos);
		Integer unoB = new Integer(1);
		Integer dosB = new Integer(2);
		Par<Integer> parInt2 = new Par<Integer>(unoB, dosB);
		assertEquals(uno, unoB);
		assertEquals(dos, dosB);
		assertEquals(parInt1.getLeft(), parInt2.getLeft());
		assertEquals(parInt1.getRight(), parInt2.getRight());
		assertEquals(parInt1, parInt2);
	}	
	
	@Test 
	public void testDosCrucesEqualsEnOrdenDebenDarParesIguales() {
		Cruce cruce1 = new Cruce(0);
		Cruce cruce2 = new Cruce(1);
		Par<Cruce> par1 = new Par<Cruce>(cruce1, cruce2);
		Cruce cruceA = new Cruce(0);
		Cruce cruceB = new Cruce(1);
		Par<Cruce> parA = new Par<Cruce>(cruceA, cruceB);
		assertEquals(par1, parA);
	}
	
	@Test 
	public void testEqualParConSiMismoDebeSerVerdadero() {
		Par<Integer> par = new Par<Integer>(new Integer(0), new Integer(1));
		assertEquals(par, par);
		Par<Cruce> parCruces = new Par<Cruce>(new Cruce(0), new Cruce(1));
		assertEquals(parCruces, parCruces);
	}
	
	@Test(expected = NoSuchElementException.class)
	public void testDestinoDeUnElementoQueNoEstaDebeDarException() {
		Cruce otro = new Cruce(3);
		Par<Cruce> parCruces = new Par<Cruce>(new Cruce(0), new Cruce(1));
		parCruces.getOpuesto(otro);
	}
	@Test
		public void testGetOpuestoRight() {
			Par<Cruce> par = new Par<Cruce>(new Cruce(1), new Cruce(2));
			Cruce cruceLeft = new Cruce(1);
			Cruce cruceRight = new Cruce(2);
			assertEquals(par.getOpuesto(cruceRight), cruceLeft);
			assertEquals(par.getOpuesto(cruceLeft), cruceRight);
		}
	
	@Test 
	public void testCompararConNullDebeDarFalso() {
		Par<Integer> par = new Par<Integer>(new Integer(0), new Integer(1));
		Par<Integer> otro = null;
		
		assertFalse(par.equals(otro));
	}
	
	@Test
	public void testCompararConObjectoQueNoSeaInstanciaDeParDebeDarFalso() {
		Par<Integer> par = new Par<Integer>(new Integer(0), new Integer(1));
		assertFalse(par.equals(new Integer(2)));
	}
	
	@Test 
	public void testUnoDeLosDosElementosEsDiferenteDebeDarFalso() {
		Integer int1 = new Integer(1);
		Integer int2 = new Integer(2);
		Integer int3 = new Integer(3);
		Par<Integer> par = new Par<Integer>(int1, int2);
		Par<Integer> parDif1 = new Par<Integer>(int1, int3);
		Par<Integer> parDif2 = new Par<Integer>(int3, int2);
		assertFalse(par.equals(parDif1));
		assertFalse(par.equals(parDif2));
	}
	
	@Test
	public void testInvertidoDebeSerDiferente() {
		Integer int1 = new Integer(1);
		Integer int2 = new Integer(2);	
		Par<Integer> par = new Par<Integer>(int1, int2);
		Par<Integer> parInvertido = new Par<Integer>(int2, int1);
		assertFalse(par.equals(parInvertido));
	}
}
