package gpschallenge.mapa;

import static org.junit.Assert.*;
import gpschallenge.mapa.Cruce.Direccion;

import org.junit.Test;

public class CruceTest {
		
	@Test 
	public void testEquals() {
		Cruce uno = new Cruce(1);
		Cruce otroIgual = new Cruce(1);
		assertEquals(uno, uno); // con sigo mismo
		assertEquals(uno,otroIgual); // con otro igual
	}
	
	@Test
	public void testEqualsConNullDebeDarFalso() {
		Cruce uno = new Cruce(1);
		assertFalse(uno.equals(null));
	}
	
	@Test 
	public void testEqualsConOtraInstanciaDebeDarFalso() {
		Cruce uno = new Cruce(1);
		assertFalse(uno.equals(new Integer(2)));
	}
	
	@Test 
	public void testToString() {
		Cruce uno = new Cruce(1);
		Cruce mucho = new Cruce(2043);
		assertEquals("Cruce: 1", uno.toString());
		assertEquals("Cruce: 2043", mucho.toString());
	}
	@Test 
	public void testIdentificadorDebeSerIGualAlPasadoEnConstructor() {
		for(int id=0; id < 10; id++) {
			Cruce cruce = new Cruce(id);
			assertEquals(cruce.getIdentificador(), id);
		}
	}
	
	@Test
	public void testAgregarCamino() {
		Cruce unCruce = new Cruce(0);
		Cruce otroCruce = new Cruce(1);
		Camino unCamino = new Camino(unCruce, otroCruce);
		unCruce.agregarCamino(unCamino, Direccion.DERECHA);
		assertEquals(unCruce.getCamino(Direccion.DERECHA), unCamino);
	}
	
	@Test
	public void testAgregarVariosCaminos() {
		Cruce unCruce = new Cruce(0);
		Cruce cruceAbajo = new Cruce(1);
		Cruce cruceDerecha = new Cruce(2);
		Camino caminoDerecha = new Camino(unCruce, cruceDerecha);
		Camino caminoAbajo = new Camino(unCruce, cruceAbajo);
		unCruce.agregarCamino(caminoDerecha, Direccion.DERECHA);
		unCruce.agregarCamino(caminoAbajo, Direccion.ABAJO);
		// SI intento agregar el mismo camino no va a pensar que 
		// tiene 3.
		unCruce.agregarCamino(caminoAbajo, Direccion.ABAJO);
		assertEquals(unCruce.getCamino(Direccion.DERECHA), caminoDerecha);
		assertEquals(unCruce.getCamino(Direccion.ABAJO), caminoAbajo);
		// Verificar que tiene 2 caminos
		assertEquals(unCruce.getCaminosSalientes().size(), 2);
	}
	@Test
	public void testGetCaminoInexistente() {
		Cruce unCruce = new Cruce(0);
		assertEquals(unCruce.getCamino(Direccion.DERECHA), null);
	}
	
	@Test
	public void testNoDebeTenerCaminosAlConstruirse() {
		Cruce unCruce = new Cruce(0);
		assertFalse(unCruce.tieneCaminoEnDireccion(Direccion.ARRIBA));
		assertFalse(unCruce.tieneCaminoEnDireccion(Direccion.ABAJO));
		assertFalse(unCruce.tieneCaminoEnDireccion(Direccion.DERECHA));
		assertFalse(unCruce.tieneCaminoEnDireccion(Direccion.IZQUIERDA));
	}

	//Similar a prueba anterior.
	@Test
	public void testCaminosSalientesDebenSerCero() {
		Cruce unCruce = new Cruce(3);
		assertEquals(unCruce.getCaminosSalientes().size(), 0);
	}
	
	
	@Test
	public void testTieneCamino() {
		Cruce unCruce = new Cruce(0);
		Cruce otroCruce = new Cruce(1);
		Camino unCamino = new Camino(unCruce, otroCruce);
		unCruce.agregarCamino(unCamino, Direccion.DERECHA);
		assertTrue(unCruce.tieneCaminoEnDireccion(Direccion.DERECHA));
	}

}
