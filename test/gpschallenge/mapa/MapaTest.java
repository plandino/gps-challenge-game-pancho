package gpschallenge.mapa;

import static org.junit.Assert.*;

import java.util.EnumMap;
import java.util.List;
import java.util.ArrayList;

import org.junit.Test;

import gpschallenge.Jugador;
import gpschallenge.dificultad.DificultadDificil;
import gpschallenge.dificultad.DificultadFacil;
import gpschallenge.dificultad.DificultadModerada;
import gpschallenge.mapa.Cruce.Direccion;
import gpschallenge.modificador.sorpresa.*;
import gpschallenge.modificador.obstaculo.*;
import gpschallenge.xmlhandler.XMLHandler;




public class MapaTest {

	@Test
	public void testCantidadCruces() {
		Mapa mapa = new Mapa(4, 4);
		mapa.inicializarCruces();
		assertEquals(mapa.getCruces().size(), 25);
	}
	
	@Test
	public void testCargarMapaJugador() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/mapaPrueba.xml", mapa);
		assertEquals(mapa.getNombreJugador(), "Ricardo");
	}
	@Test
	public void testCargarDimensionesMapa() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/mapaPrueba.xml", mapa);
		assertEquals(mapa.getAncho(), 3);
		assertEquals(mapa.getAlto(), 3);
	}
	@Test
	public void testCargarCruces() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/mapaPrueba.xml", mapa);
		List<Cruce> esperado = new ArrayList<Cruce>();
		
		esperado.add(new Cruce(0));
		esperado.add(new Cruce(1));
		esperado.add(new Cruce(2));
		esperado.add(new Cruce(3));
		esperado.add(new Cruce(4));
		esperado.add(new Cruce(5));
		esperado.add(new Cruce(6));
		esperado.add(new Cruce(7));
		esperado.add(new Cruce(8));
		
		assertEquals(mapa.getCruces(), esperado);
	}
	@Test
	public void testCargarCaminos() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		Cruce unCruce = mapa.getCruces().get(0);
		Cruce otroCruce = mapa.getCruces().get(1);
		Camino unCamino = new Camino(unCruce, otroCruce);
		assertEquals(mapa.getCaminos().get(0), unCamino);
		Cruce otroCruceMas = mapa.getCruces().get(2);
		Camino otroCamino = new Camino(otroCruce, otroCruceMas);
		assertEquals(mapa.getCaminos().get(1), otroCamino);
	}
	@Test
	public void testCaminosCargadosSeDeberianAgregarCorrectamenteACadaCruce() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		assertNotNull(mapa.getCruces().get(0).getCamino(Direccion.DERECHA));
		assertNotNull(mapa.getCruces().get(1).getCamino(Direccion.IZQUIERDA));
		Cruce unCruce = mapa.getCruces().get(1);
		assertEquals(unCruce.getCamino(Direccion.IZQUIERDA).getDestino(unCruce), mapa.getCruces().get(0));	
	}
	@Test
	public void testCargarCaminosDesdeXML() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/mapaPrueba.xml", mapa);
		List<Camino> caminos = new ArrayList<Camino>();
		List<Cruce> cruces = mapa.getCruces();
		caminos.add(new Camino(cruces.get(0), cruces.get(1)));
		caminos.add(new Camino(cruces.get(1), cruces.get(2)));
		caminos.add(new Camino(cruces.get(3), cruces.get(4)));
		caminos.add(new Camino(cruces.get(4), cruces.get(5)));
		caminos.add(new Camino(cruces.get(6), cruces.get(7)));
		caminos.add(new Camino(cruces.get(7), cruces.get(8)));
		caminos.add(new Camino(cruces.get(0), cruces.get(3)));
		caminos.add(new Camino(cruces.get(3), cruces.get(6)));
		caminos.add(new Camino(cruces.get(1), cruces.get(4)));
		caminos.add(new Camino(cruces.get(4), cruces.get(7)));
		caminos.add(new Camino(cruces.get(2), cruces.get(5)));
		caminos.add(new Camino(cruces.get(5), cruces.get(8)));
		
		assertEquals(caminos, mapa.getCaminos());
		
		//Se fija si los caminos dirigen correctamente (en sentido derecha y abajo).
		
		assertEquals(cruces.get(0).getCamino(Direccion.DERECHA), caminos.get(0));
		assertEquals(cruces.get(1).getCamino(Direccion.DERECHA), caminos.get(1));
		assertEquals(cruces.get(3).getCamino(Direccion.DERECHA), caminos.get(2));
		assertEquals(cruces.get(4).getCamino(Direccion.DERECHA), caminos.get(3));
		assertEquals(cruces.get(6).getCamino(Direccion.DERECHA), caminos.get(4));
		assertEquals(cruces.get(7).getCamino(Direccion.DERECHA), caminos.get(5));
		assertEquals(cruces.get(0).getCamino(Direccion.ABAJO), caminos.get(6));
		assertEquals(cruces.get(3).getCamino(Direccion.ABAJO), caminos.get(7));
		assertEquals(cruces.get(1).getCamino(Direccion.ABAJO), caminos.get(8));
		assertEquals(cruces.get(4).getCamino(Direccion.ABAJO), caminos.get(9));
		assertEquals(cruces.get(2).getCamino(Direccion.ABAJO), caminos.get(10));
		assertEquals(cruces.get(5).getCamino(Direccion.ABAJO), caminos.get(11));

	}
	@Test
	public void testNoDeberiaCargarCaminosInvalidosXML() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		assertNull(mapa.getCruces().get(0).getCamino(Direccion.IZQUIERDA));
	}
	@Test
	public void testCargarMapaRectangularDimensiones() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		assertEquals(mapa.getAncho(), 5);
		assertEquals(mapa.getAlto(), 4);
	}
	@Test
	public void testCargarCrucesMapaRectangular() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		List<Cruce> esperado = new ArrayList<Cruce>();
		esperado.add(new Cruce(0));
		esperado.add(new Cruce(1));
		esperado.add(new Cruce(2));
		esperado.add(new Cruce(3));
		esperado.add(new Cruce(4));
		esperado.add(new Cruce(5));
		esperado.add(new Cruce(6));
		esperado.add(new Cruce(7));
		esperado.add(new Cruce(8));
		esperado.add(new Cruce(9));
		esperado.add(new Cruce(10));
		esperado.add(new Cruce(11));
		esperado.add(new Cruce(12));
		esperado.add(new Cruce(13));
		esperado.add(new Cruce(14));
		esperado.add(new Cruce(15));
		esperado.add(new Cruce(16));
		esperado.add(new Cruce(17));
		esperado.add(new Cruce(18));
		esperado.add(new Cruce(19));
		
		assertEquals(mapa.getCruces(), esperado);

	}
	
	@Test
	public void testCargarSorpresasDeCamino() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		assertEquals(mapa.getCaminos().get(5).getEfectosEnOrden().get(0).getClass(), (new SorpresaCambioDeVehiculo()).getClass());	
	}
	
	@Test
	public void testCargarObstaculosDeCamino() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		
		assertNotNull(mapa.getCaminos());
		assertNotNull(mapa.getCaminos().get(4).getEfectosEnOrden());
		assertTrue(mapa.getCaminos().get(4).getEfectosEnOrden().size() > 1);
		//mapa.getCaminos().size() > 0
		//System.out.println(mapa.getCaminos().get(4).getEfectos().get(1).getClass().toString());
		//assertEquals(mapa.getCaminos().get(4).getModificadoresObtenibles().get(1), "gpschallenge.modificador.obstaculo.Piquete");
		//assertEquals(mapa.getCaminos().get(4).getEfectos().get(1).getClass(), (new Piquete(new Auto())).getClass()); 
		
	}
	
	@Test
	public void testCargarCaminosConSorpresasCorrectamente() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		// TODO checkear que cruce ponerle a getEfectos(Cruce cruceActual)
		assertEquals(mapa.getCruces().get(0).getCamino(Direccion.DERECHA).getEfectosEnOrden().get(0).getClass(), 
				SorpresaFavorable.class);
		List<Cruce> cruces = mapa.getCruces();
		assertEquals(cruces.get(5).getCamino(Direccion.DERECHA).getEfectosEnOrden().get(1).getClass(), Piquete.class);
		assertEquals(cruces.get(6).getCamino(Direccion.DERECHA).getEfectosEnOrden().get(0).getClass(), SorpresaCambioDeVehiculo.class);
		assertEquals(cruces.get(7).getCamino(Direccion.IZQUIERDA).getEfectosEnOrden().get(0).getClass(), SorpresaCambioDeVehiculo.class);
		assertSame(cruces.get(6).getCamino(Direccion.DERECHA).getEfectosEnOrden().get(0), 
				cruces.get(7).getCamino(Direccion.IZQUIERDA).getEfectosEnOrden().get(0));
	}
	
	@Test 
	public void testCargarNullsEnCamino() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		assertNull(mapa.getCruces().get(0).getCamino(Direccion.DERECHA).getEfectosEnOrden().get(1));
		assertNull(mapa.getCruces().get(5).getCamino(Direccion.DERECHA).getEfectosEnOrden().get(0));
	}

	
	@Test 
	public void testGetPosicionMetaDebeDevolverUnaMetaContenidaEnLosCrucesDelMapa() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		Cruce cruceMeta = mapa.getPosicionMeta();
		assertTrue(mapa.getCruces().contains(cruceMeta));
	}
	
	//// INTEGRACION
	@Test 
	public void testObtenerCaminosAPartirDeOtros() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);	
		
		// primero seteo el vehiculo en un cruce conocido porque conozco el mapa.
		Cruce posInicial = mapa.getCruces().get(6);
		mapa.setPosicionVehiculo(posInicial);
		Cruce posVehiculo = mapa.getPosicionVehiculo();
		// compruebo que sean los mismos por las dudas.
		assertEquals(posInicial, posVehiculo);
		assertEquals(posInicial.getCaminosSalientes(), posVehiculo.getCaminosSalientes());
		// tiene que haber cuatro caminos.
		assertEquals(posInicial.getCaminosSalientes().size(), 4);
		
		// Me muevo hacia arriba
		Cruce cruceDestino = posVehiculo.getCamino(Direccion.ARRIBA).getDestino(posVehiculo);
		// verifico que sea el cruce que espero
		assertEquals(cruceDestino, new Cruce(1));
		Cruce similDestino = mapa.getCruces().get(cruceDestino.getIdentificador());
		assertEquals(similDestino.getCaminosSalientes().size(), 3);
		assertEquals(similDestino, cruceDestino);
		
		mapa.setPosicionVehiculo(cruceDestino);
		
	}
	@Test
	public void testCargarDificultadMapa() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		DificultadFacil dificultadFacil = new DificultadFacil("Facil");
		assertEquals(mapa.getDificultad().getClass(), dificultadFacil.getClass());
		assertEquals(mapa.getDificultad().getNombre(), "Facil");
	}
	@Test
	public void testCargarDificultadMapaRectangular() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/mapaPrueba.xml", mapa);
		assertEquals(mapa.getDificultad().getNombre(), "Facil");
	}
	
	@Test
	public void testCargarLimiteMovimientos() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		assertEquals(mapa.getLimiteMovimientos(), 20.00, 1e-15); //el 1e-15 es el epsilon (maximo de diferencia entre los dos numeros)
	}
	
	public void testGuardarMapa() { //Usado para generar los XML de manera rapida.
		Mapa mapa = new Mapa();
		ArrayList<Cruce> cruces = mapa.getCruces();
		cruces.add(new Cruce(5));
		cruces.add(new Cruce(6));
		cruces.add(new Cruce(7));
		List<Camino> caminos = mapa.getCaminos();
		caminos.add(new Camino(cruces.get(0), cruces.get(1) ));
		caminos.add(new Camino(cruces.get(0), cruces.get(2) ));
		caminos.get(0).getEfectosDesde(cruces.get(0)).add(new SorpresaFavorable());
		cruces.get(0).agregarCamino(caminos.get(0), Direccion.DERECHA);
		cruces.get(1).agregarCamino(caminos.get(0), Direccion.IZQUIERDA);
		cruces.get(0).agregarCamino(caminos.get(1), Direccion.ABAJO);
		cruces.get(2).agregarCamino(caminos.get(1), Direccion.ARRIBA);
		XMLHandler xmlh = new XMLHandler();
		mapa.setNombreJugador("soyunaprueba");
		xmlh.guardarArchivo(mapa, "test/gpschallenge/mapa/");
		
	}
	
	@Test
	public void testAsignoFalseALaVisibilidadENTodosLosCaminosDelMapa(){
		// Para verificar si le seteo false a todos los caminos del mapa, primero tengo que poner
		// algunos en true, porque por definicion cuando se crea un camino, se crea con false
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/mapaPrueba.xml", mapa);
		List<Camino> listaDeCaminos = mapa.getCaminos();
		listaDeCaminos.get(2).setVisible(true);
		listaDeCaminos.get(4).setVisible(true);
		
		assertEquals(listaDeCaminos.get(2).estaVisible(), true);
		assertEquals(listaDeCaminos.get(4).estaVisible(), true);
		
		mapa.setFalseAVisibilidadEnTodosLosCaminos();
		
		assertEquals(listaDeCaminos.get(2).estaVisible(), false);
		assertEquals(listaDeCaminos.get(4).estaVisible(), false);
		
	}
	
	@Test
	public void testAsignaTrueALosCaminosSalientesDelCruceDondeEstaElVehiculoConVisibilidadUno(){
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		Cruce cruceAdondePosicionarElVehiculo = (mapa.getCruces()).get(8);
		mapa.setPosicionVehiculo(cruceAdondePosicionarElVehiculo);
		
		mapa.setCaminosVisibleCercanosAlVehiculo(cruceAdondePosicionarElVehiculo, 1);
		EnumMap<Direccion, Camino> caminosSalientes = cruceAdondePosicionarElVehiculo.getCaminosSalientes();

		assertEquals(true, caminosSalientes.get(Direccion.ABAJO).estaVisible());
		assertEquals(true, caminosSalientes.get(Direccion.ARRIBA).estaVisible());
		assertEquals(true, caminosSalientes.get(Direccion.IZQUIERDA).estaVisible());
		assertEquals(true, caminosSalientes.get(Direccion.DERECHA).estaVisible());

	}
	
	@Test
	public void testAsignaTrueALosCaminosSalientesDelCruceDondeEstaElVehiculoConVisibilidadDos(){
		int topeVisibilidad = 2;
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		Cruce cruceAdondePosicionarElVehiculo = (mapa.getCruces()).get(12);
		mapa.setPosicionVehiculo(cruceAdondePosicionarElVehiculo);
		
		mapa.setCaminosVisibleCercanosAlVehiculo(cruceAdondePosicionarElVehiculo, topeVisibilidad);
		EnumMap<Direccion, Camino> caminosSalientes = cruceAdondePosicionarElVehiculo.getCaminosSalientes();

		Cruce cruceArriba = caminosSalientes.get(Direccion.ARRIBA).getDestino(cruceAdondePosicionarElVehiculo);
		// Se va dos cruces alejado del Cruce donde esta posicionado el vehiculo y verifica que todos los caminos
		// salientes del segundo cruce, esten visibles
		EnumMap<Direccion, Camino> caminosSalientesSegundoNivel = cruceArriba.getCaminosSalientes();

		assertEquals(true, caminosSalientesSegundoNivel.get(Direccion.ABAJO).estaVisible());
		assertEquals(true, caminosSalientesSegundoNivel.get(Direccion.ARRIBA).estaVisible());
		assertEquals(true, caminosSalientesSegundoNivel.get(Direccion.IZQUIERDA).estaVisible());
		assertEquals(true, caminosSalientesSegundoNivel.get(Direccion.DERECHA).estaVisible());
	}
	@Test
	public void testCargarDificultadModerada() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/mapamoderado.xml", mapa);
		DificultadModerada dif = new DificultadModerada("Moderada");
		assertEquals(mapa.getDificultad().getNombre(), dif.getNombre());
	}
	@Test
	public void testCargarMetaDeArchivo() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		assertNull(mapa.getPosicionMeta());
		xmlh.cargarPartidaGuardada("test/gpschallenge/mapa/mapamoderado.xml", mapa);
		assertNotNull(mapa.getPosicionMeta());
		Cruce metaEsperada = new Cruce(59);
		assertEquals(mapa.getPosicionMeta(), metaEsperada);
	}
	@Test
	public void testCargarPosicionActualDeArchivo() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		assertNull(mapa.getPosicionVehiculo());
		xmlh.cargarPartidaGuardada("test/gpschallenge/mapa/mapamoderado.xml", mapa);
		assertNotNull(mapa.getPosicionVehiculo());
		Cruce posicionEsperada = new Cruce(12);
		assertEquals(posicionEsperada, mapa.getPosicionVehiculo());
	}
	@Test
	public void testGuardarMeta() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarArchivo("test/gpschallenge/mapa/maparectangulartest.xml", mapa);
		mapa.setPosicionMeta(mapa.getCruces().get(18));
		xmlh.guardarArchivo(mapa, "test/gpschallenge/mapa/");
	}
	@Test
	public void testCargarMovimientosDePartidaGuardada() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		xmlh.cargarPartidaGuardada("test/gpschallenge/mapa/mapamoderado.xml", mapa);
		assertEquals(mapa.getJugador().getContador().getCantidadDeMovimientos(), 1.0, 1e-15);
	}
	@Test
	public void testGuardarMovimientos() {
		Mapa mapa = new Mapa(4, 4);
		mapa.inicializarCruces();
		assertEquals(mapa.getCruces().size(), 25);
		XMLHandler xmlh = new XMLHandler();
		mapa.setJugador(new Jugador("Pablito"));
		mapa.setDificultad(new DificultadDificil("Dificil"));
		mapa.setLimiteMovimientos(150);
		mapa.getJugador().getContador().setCantidad(0);
		mapa.getJugador().sumaDeMovimientos(2.00);
		mapa.setPosicionMeta(mapa.getCruces().get(5));
		mapa.setPosicionVehiculo(mapa.getCruces().get(9));
		xmlh.guardarArchivo(mapa, "test/gpschallenge/mapa/testmovimientos");
		
		Mapa otroMapa = new Mapa();
		xmlh.cargarPartidaGuardada("test/gpschallenge/mapa/testmovimientosPablito.xml", otroMapa);
		assertEquals(otroMapa.getJugador().getContador().getCantidadDeMovimientos(), mapa.getJugador().getContador().getCantidadDeMovimientos(), 1e-15);
		
	}
}
