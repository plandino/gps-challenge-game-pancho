package gpschallenge;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Map.Entry;

import gpschallenge.mapa.*;
import gpschallenge.mapa.Cruce.Direccion;
import gpschallenge.modificador.*;
import gpschallenge.modificador.obstaculo.*;
import gpschallenge.modificador.sorpresa.*;
import gpschallenge.ranking.Puntaje;
import gpschallenge.ranking.Ranking;
import gpschallenge.vehiculo.*;
import gpschallenge.xmlhandler.XMLHandler;

import org.junit.Before;
import org.junit.Test;

public class PartidaTest {
	
	private Mapa mapa;
	private Jugador jugador;
	private Partida partida;
	private final double DELTA = 1e-15;
	private final String rutaDelRanking = "test/gpschallenge/ranking/rankingPruebas.csv";
	
	@Before
	public void setUp() {
		mapa = new Mapa(3,3);
		XMLHandler xmlh = new XMLHandler();
		jugador = new Jugador();
		partida = new Partida(mapa, jugador);
		xmlh.cargarArchivo("test/gpschallenge/mapa/mapaPrueba.xml", mapa);
		Ranking unRanking = new Ranking(rutaDelRanking);
		partida.setRanking(unRanking);
	}
	
	@Test 
	public void testPosicionDeVehiculoDebePoderSerIgualUnaVezSeteada() {
		Cruce primerCruce = mapa.getCruces().get(1);
		mapa.setPosicionVehiculo(primerCruce);
		Cruce cruce = partida.getPosicionVehiculo();
		assertEquals(primerCruce.getIdentificador(), cruce.getIdentificador());
	}
	
	@Test
	public void testProcesarModificadoresConUnCambioDeVehiculoDebeAndar() {		
		Vehiculo vehiculoOriginal = partida.getJugador().getVehiculo();
		Vehiculo vehiculoEsperado = (Vehiculo) partida.getJugador().getVehiculo().vehiculoDeCambio();
		
		ArrayList<IEfectoObtenible> listaModificadores = new ArrayList<IEfectoObtenible>();
		listaModificadores.add(new SorpresaCambioDeVehiculo());

		partida.procesarModificadores(listaModificadores);
		
		Vehiculo vehiculoObtenido = partida.getJugador().getVehiculo();
		
		// Esperado es correcto
		assertEquals(vehiculoEsperado.getClass(), vehiculoObtenido.getClass());
		// Distinto del original
		assert(vehiculoOriginal.getClass() != vehiculoEsperado.getClass());
	}
	
	
	   @Test 
	   public void testAutoEncuentraPiqueteYSopresaCambioDeAutoPeroNoPuedePasarYSeQuedaEnAuto() {
		//partida.getJugador().setVehiculo(new Auto());  //asegurarse de que sea un Auto
		Vehiculo vehiculoOriginal = partida.getJugador().getVehiculo();
		Cruce posicionOriginal = partida.getPosicionVehiculo();
		
		ArrayList<IEfectoObtenible> listaModificadores = new ArrayList<IEfectoObtenible>();
		listaModificadores.add(new Piquete());
		listaModificadores.add(new SorpresaCambioDeVehiculo());
		
		partida.procesarModificadores(listaModificadores);
		
		// debe apuntar al mismo objeto que antes porque no cambia nada.
		assertSame(vehiculoOriginal, partida.getJugador().getVehiculo());
		
		assertEquals(posicionOriginal, partida.getPosicionVehiculo());
	}
	   
    @Test
    public void testMotoDebeSerPenalizadaCon2MovimientosEnPozo() {
    	partida.getJugador().setVehiculo(new Moto());
    	ContadorDeMovimientos contador = partida.getJugador().getContador();
    	contador.setCantidad(1.0);
    	assertEquals(contador.getCantidadDeMovimientos(), 1.0, DELTA);
    	ArrayList<IEfectoObtenible> listaModificadores = new ArrayList<IEfectoObtenible>();
    	listaModificadores.add(new Pozo());
    	
    	partida.procesarModificadores(listaModificadores);
    	
    	// le suma 3 por ser moto y 1 mas por movimiento exitoso.  1 + 3 + 1 = 5;
    	assertEquals(5.0, partida.getJugador().getContador().getCantidadDeMovimientos(), DELTA);
    }
	   
    @Test 
    public void testSorpresaDesfavorableSumaPorcentajeDeMovimientosHechos() {
    	partida.getJugador().getContador().setCantidad(10.0);
    	assertEquals(partida.getJugador().getContador().getCantidadDeMovimientos(), 10.0, DELTA);
    	
    	ArrayList<IEfectoObtenible> listaModificadores = new ArrayList<IEfectoObtenible>();
    	listaModificadores.add(new SorpresaDesfavorable());
    	
    	partida.procesarModificadores(listaModificadores);
    	
    	assertEquals(10.0 * 1.25 + 1, partida.getJugador().getContador().getCantidadDeMovimientos(), DELTA);
    }
	// Triviales
	@Test
	public void testGetMapa() {
		assertEquals(partida.getMapa(), mapa);
	}
	
	@Test 
	public void testGetJugador() {
		assertEquals(partida.getJugador(), jugador);
	}
	
	@Test 
	public void testGetVehiculo() {
		assertEquals(partida.getVehiculo(), jugador.getVehiculo());
	}	
	
	public void testGetDestino() {
		Cruce uno = new Cruce(1);
		Cruce dos = new Cruce(2);
		Cruce tres = new Cruce(3);
		Camino unodos = new Camino(uno, dos);
		@SuppressWarnings("unused")
		Camino unotres = new Camino(uno, tres);
		@SuppressWarnings("unused")
		Camino dostres = new Camino(dos, tres);
		// Esta forma de agregar los caminos DOS veces es una porqueria
		// El constructor de Camino debería hacer la conexión.
		// y no debe ser un ENUM! 
		//Debe ser un string para que sea fucking extensible!
		uno.agregarCamino(unodos, Direccion.ARRIBA);
		dos.agregarCamino(unodos, Direccion.ABAJO);
		
		dos.agregarCamino(unodos, Direccion.IZQUIERDA);
		tres.agregarCamino(unodos, Direccion.DERECHA);
		
		uno.agregarCamino(unodos, Direccion.ARRIBA);
		tres.agregarCamino(unodos, Direccion.ABAJO);
		
		ArrayList<Cruce> listaCruces = new ArrayList<Cruce>();
		listaCruces.add(uno);
		listaCruces.add(dos);
		listaCruces.add(tres);
		for(Cruce cruce : listaCruces) {
			for(Entry<Direccion, Camino> entry: cruce.getCaminosSalientes().entrySet()) {
				Camino camino = entry.getValue();
				assertEquals(camino, cruce.getCamino(entry.getKey()));
			}
		}
		
		//No se puede probar con facilidad el opuesto porque no 
		//se sabe la direccion opuesta a la que es
	}
	
	@Test
	public void testEnumMapDebeSerElSalienteDelVehiculo() {
		assertSame(partida.getPosicionVehiculo().getCaminosSalientes(), partida.getCaminosPosibles());
	}
	
	@Test
	public void testHaGanadoPartida() {
		assertFalse(partida.partidaGanada());
		partida.getMapa().setPosicionVehiculo(partida.getMapa().getPosicionMeta());
		assertTrue(partida.partidaGanada());
	}
	
	@Test
	public void testHaPerdidoPartida() {
		assertFalse(partida.partidaPerdida());
		partida.getJugador().getContador().setCantidad(partida.getJugador().getContador().getLimiteDeMovimientos() + 1);
		assertTrue(partida.partidaPerdida());
	}
	
	@Test
	public void testSiListaModificadoresEnProcesarModificadorTieneSoloNullsDebeSerMovimientoExitosoYSumarUno()
	{
		ContadorDeMovimientos contador = partida.getJugador().getContador();
		double movInicial = contador.getCantidadDeMovimientos();
		ArrayList<IEfectoObtenible> listaModificadores = new ArrayList<IEfectoObtenible>();
		listaModificadores.add(null);
		listaModificadores.add(null);
		boolean movimientoExitoso = partida.procesarModificadores(listaModificadores);
		assertEquals(movInicial + 1, contador.getCantidadDeMovimientos(), this.DELTA);
		assertTrue(movimientoExitoso);
	}
	
	
	@Test
	public void testSalvarPartidaDebeCrearArchivoConNombreEnRutaIndicada() {
		String nombreDesignado = "unnombre";
		partida.getMapa().setNombreJugador(nombreDesignado);
		partida.salvarPartida();
		String path = "src/gpschallenge/recursos/partidas_guardadas/" + nombreDesignado + ".xml";
		//System.out.println(path);
	    File f = new File(path);
		assertTrue(f.exists());
	}
	
	
	@Test
	public void testAsignaBienElRankingCuandoGana(){
		
		Cruce posicionMeta = mapa.getPosicionMeta();
		mapa.setPosicionVehiculo(posicionMeta);
		
		Ranking miRanking = partida.getRanking();
		miRanking.colocarEnElRanking(new Puntaje("Juan", 25));
		
		ArrayList<Puntaje> unArrayDePuntajes = miRanking.getListaPuntajes();
		// Le asigno un puntaje mayor que todos, el puntaje maximo cargado por defecto en test/gpschallenge/ranking/rankingPruebas.csv
		// es Roberto, 20.0
		assertEquals(unArrayDePuntajes.get(0).getNombre(), "Juan");
		assertEquals(unArrayDePuntajes.get(0).getPuntaje(), 25, 0.1);
		borrarRankingSinUsar(rutaDelRanking);
	}
	
	@Test
	public void testFuncionaBienElMetodoGuardarPuntajeSiGana() {
		Cruce posicionMeta = mapa.getPosicionMeta();
		mapa.setPosicionVehiculo(posicionMeta);
		partida.guardarPuntajeSiGana();
		Ranking miRanking = partida.getRanking();
		
		ArrayList<Puntaje> unArrayDePuntajes = miRanking.getListaPuntajes();
		
		String nombre = mapa.getNombreJugador();
		
		double puntaje = partida.getJugador().getPuntaje(partida.getDificultad());

		assertEquals(unArrayDePuntajes.get(0).getNombre(), nombre);
		assertEquals(unArrayDePuntajes.get(0).getPuntaje(), puntaje, 0.1);
		borrarRankingSinUsar(rutaDelRanking);

	}
	
	@Test
	public void testColocarBienElPuntajeEnElRanking(){
		Ranking ranking = partida.getRanking();
		ranking.colocarEnElRanking(new Puntaje("Fede",9));
		ArrayList<Puntaje> unArrayDePuntajes = ranking.getListaPuntajes();
		
		assertEquals((unArrayDePuntajes.get(0).getNombre()),"Fede");
        assertEquals((unArrayDePuntajes.get(0).getPuntaje()), 9 , 0.1);
        
        borrarRankingSinUsar(rutaDelRanking);
		
	}
	
	// Este se usa para borrar el archivo que se crea debido a que se introdujo una ruta invalida
	public void borrarRankingSinUsar(String ruta){
			File fileABorrar = new File(ruta);
			fileABorrar.delete();
		}
	
	/*@Test
	public void testMoverJugadorAMetaDebeGuardarPuntaje() {
		// poner la meta en un camino al lado del jugador.  0 -> 1 (por derecha)
		Cruce posInicial = partida.getMapa().getCruces().get(0);
		
		
		Camino caminoAMeta = posInicial.getCamino(Direccion.DERECHA);
		partida.getMapa().setPosicionMeta(partida.getMapa().getCruces().get(1));
		partida.getMapa().setPosicionVehiculo(posInicial);
		System.out.println(caminoAMeta.getCruces().getLeft() + " " + caminoAMeta.getCruces().getRight());
		
		
     	partida.getMapa().setNombreJugador("campeon");
		partida.getJugador().getContador().setCantidad(0);
		partida.getJugador().getContador().setLimiteDeMovimientos(100);
		
		partida.moverJugadorPor(caminoAMeta);
		
	//	System.out.println(unRanking.devolverRanking().get(0));
		}*/
}


