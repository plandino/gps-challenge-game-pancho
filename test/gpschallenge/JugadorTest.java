package gpschallenge;

import static org.junit.Assert.*;
import gpschallenge.dificultad.Dificultad;
import gpschallenge.dificultad.DificultadModerada;
import gpschallenge.mapa.Mapa;

import org.junit.Test;

public class JugadorTest {
	
	private final double DELTA = 1e-15;

	@Test 
	public void testGetPuntaje() {
		Partida partida = new Partida(new Mapa(), new Jugador());
		double movimientosRealizados = 10.0;
		double limiteMovimientos = 30.0;
		ContadorDeMovimientos contador = partida.getJugador().getContador();
		contador.setCantidad(movimientosRealizados);
		contador.setLimiteDeMovimientos(limiteMovimientos);
		Dificultad dif = new DificultadModerada("mod");
		partida.setDificultad(dif);
		assertEquals(40.0, partida.getJugador().getPuntaje(partida.getDificultad()), this.DELTA);
	}
	
}
