GPS Challenge Game

para probar usar IDE o ant con el siguiente comando:

    ant ejecutar.aplicacion

GPS es un juego de estrategia por turnos. El escenario es una ciudad y el objetivo, guiar un vehículo a la meta en la menor cantidad de movimientos posibles. 
A lo largo del recorrido uno puede encontrarse con distintos obstaculos que nos sumaran
movimientos cuando pasemos por encima de ellos. Tambien habra sorpresas desperdigadas por el mapa, cuyo contenido no se conoce a simple vista.
Al terminar una partida se calculará el puntaje que obtuvo a partir de los movimientos usados para llegar a la meta.
Los mapas de las distintas dificultades varían en tamanio y cantidad de obstaculos, pero tambien dan un mejor puntaje al ganar!

Instrucciones de uso:
Al iniciar la aplicacion le preguntara si es un usuario que ya ha jugado al juego o si es la primera vez que entra. Si es su primera vez ingrese su nombre y proceda a comenzar una nueva partida eligiendo el nivel de dificultad y el vehículo con el que desea iniciar.
Si ya ha jugado, seleccione su usuario de la lista y puede retomar su partida anterior o iniciar una nueva.
Tambien puede revisar los puntajes de los 10 mejores jugadores.

Para moverse por el mapa use la ayuda de los botones disponibles en la pantalla, o bien las flechas direccionales del teclado. A la izquierda verá un contador
de movimientos actuales y el máximo que puede usar antes de perder la partida. 
Para mejorar la experiencia, solo se le permite ver un pequeño área alrededor de su posición actual y la meta.

