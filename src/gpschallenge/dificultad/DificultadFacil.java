package gpschallenge.dificultad;

public class DificultadFacil extends Dificultad {

	public DificultadFacil(String nombre) {
		super(nombre);
	}

	@Override
	public	double puntajeEnBaseAMovimientos(double realizados, double sobrantes) {
		return sobrantes - realizados;
	}

}
