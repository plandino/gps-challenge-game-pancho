package gpschallenge.dificultad;

import gpschallenge.mapa.Cruce;

/**
 *  clase abstracta de Dificultad que calcula un puntaje en base a movimientos.
 */
public abstract class Dificultad {
	private String nombre;
	
	public Dificultad(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public abstract double puntajeEnBaseAMovimientos(double realizados, double limites);
	
	public boolean equals(Object c2) {
		if (c2 == null) return false;
		if (c2 == this) return true;
		if (!(c2 instanceof Cruce)) return false;
		
		Dificultad otraDif = (Dificultad)c2;
		if (otraDif.getClass() == this.getClass() &&
			otraDif.getNombre().equals(this.getNombre())) return true;
		return false;
	}
	
	/**
	 * Genera un objeto dificultad a partir de un string que representa el nombre de la clase correspondiente.
	 * @param dificultad Un String correspondiente al nombre de la clase de la Dificultad a crear.
	 * @return Una instancia de la clase correspondiente al String ingresado.
	 */
	public static Dificultad DificultadAPartirDeString(String dificultad) {
		if (dificultad.equals("Facil")) return new DificultadFacil("Facil");
		if (dificultad.equals("Moderada")) return new DificultadModerada("Moderada");
		if (dificultad.equals("Dificil")) return new DificultadDificil("Dificil");
		return null;
	}
}
