package gpschallenge.dificultad;

public class DificultadDificil extends Dificultad {

	public DificultadDificil(String nombre) {
		super(nombre);
	}

	@Override
	public	double puntajeEnBaseAMovimientos(double realizados, double sobrantes) {
		return 3.0 * (sobrantes - realizados);
	}

}
