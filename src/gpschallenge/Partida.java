package gpschallenge;

import java.util.ArrayList;
import java.util.EnumMap;

import gpschallenge.dificultad.*;
import gpschallenge.mapa.*;
import gpschallenge.modificador.*;
import gpschallenge.ranking.Puntaje;
import gpschallenge.ranking.Ranking;
import gpschallenge.vehiculo.Vehiculo;
import gpschallenge.xmlhandler.XMLHandler;

/**
 * Cada partida del juego, con su jugador y su mapa correspondientes.
 * @author Alex
 *
 */
public class Partida extends java.util.Observable {
	/**
	 * El mapa correspondiente a la partida
	 */
	private Mapa miMapa;
	/**
	 * El jugador que esta jugando esta partida
	 */
	private Jugador miJugador;
	private Ranking miRaking;
	private ArrayList<IEfectoObtenible> ultimosEfectos;
	
	/**
	 * Crea una partida asignandole un jugador y un mapa.
	 * @param mapa
	 * @param jugador
	 */
	public Partida(Mapa mapa, Jugador jugador){
		ultimosEfectos = new ArrayList<IEfectoObtenible>();
		miMapa = mapa;
		miJugador = jugador;
		miMapa.setJugador(jugador);
		miMapa.setDificultad(new DificultadFacil("Facil"));
		jugador.getContador().setLimiteDeMovimientos(miMapa.getLimiteMovimientos());
		actualizarObservadores();
	}
	
	/**
	 * Fija el ranking de puntajes.
	 * @param ranking
	 */
	public void setRanking(Ranking ranking){
		this.miRaking = ranking;
	}
	
	/**
	 * Devuelve el ranking de puntajes
	 * @return
	 */
	public Ranking getRanking(){
		return this.miRaking;
	}
	
	/**
	 * Intenta mover un jugador por un cierto camino, procesando los Efectos que pueden estar contenidos en el mismo y 
	 * moviendolo si es posible, con su penalización, si existe.
	 * @param camino
	 */
	// renombrar a intentarMoverJugadorPor
	public void moverJugadorPor(Camino camino) {
		// obtengo la posicion actual del vehiculo 
		Cruce pos = miMapa.getPosicionVehiculo();
		// obtengo el destino desde esa posicion tomando el camino indicado
		Cruce destino = camino.getDestino(pos);
		
		ArrayList<IEfectoObtenible> listaEfectos = camino.getEfectosDesde(pos);
		boolean exitoso = procesarModificadores(listaEfectos);
		// cambio la posicion del vehiculo en consecuencia.
		if(exitoso) {
			miMapa.setPosicionVehiculo(destino);
		}
		
		guardarPuntajeSiGana();
		
		// primero guarda la partida y despues notifica a los observadores
		// En el caso de que la partida se haya ganado, la partida se va a borrar
		salvarPartida();
		actualizarObservadores();
	}
	/**
	 * Guarda el puntaje del jugador si este gano la partida.
	 */
	public void guardarPuntajeSiGana() {
		if(this.partidaGanada()) {
			Puntaje puntaje = new Puntaje(miMapa.getNombreJugador(), miJugador.getPuntaje(this.getDificultad()));
			miJugador.getPuntaje(this.getDificultad());
			miRaking.colocarEnElRanking(puntaje);
		}
	}
	/**
	 * Salva la partida actual para poder retomarla si se desea.
	 */
	void salvarPartida() {
		XMLHandler xmlh = new XMLHandler();
		String ruta = "src/gpschallenge/recursos/partidas_guardadas/";
		xmlh.guardarArchivo(getMapa(), ruta);
	}
	
	/**
	 * Se fija si el jugador ha superado el limite de movimientos posibles.
	 * @return
	 */
	public boolean partidaPerdida() {
		
		return this.getJugador().getContador().noTieneMasMovimientosDisponibles();
	}
	
	/**
	 * Se fija si el jugador ha llegado a la meta.
	 * @return
	 */
	public boolean partidaGanada() {
		
		return miMapa.getPosicionVehiculo().equals(miMapa.getPosicionMeta());
	}

	public Cruce getPosicionVehiculo() {
		
		return miMapa.getPosicionVehiculo();
	}

    //must notify observers
	public void actualizarObservadores() {
		setChanged();
		notifyObservers();
	}
	/**
	 * Devuelve los caminos posibles para la posicion donde se encuentra el vehiculo dentro del mapa.
	 * @return
	 */
	public EnumMap<Cruce.Direccion,Camino> getCaminosPosibles() {
		return getPosicionVehiculo().getCaminosSalientes();
	}
	
	/**
	 * Devuelve el mapa con el que se esta jugando la partida.
	 * @return
	 */
	public Mapa getMapa() {
		
		return miMapa;
	}
	
	/**
	 * Devuelve el vehiculo que el jugador esta usando
	 * @return
	 */
	public Vehiculo getVehiculo() {
		
		return miJugador.getVehiculo();
	}
	
	/**
	 * Dada una lista de efectos, los procesa para saber si el vehiculo puede o no moverse y con que penalizacion.
	 * @param listaModificadores
	 * @return
	 */
	public boolean procesarModificadores(
			ArrayList<IEfectoObtenible> listaModificadores) {
		boolean movimientoExitoso = true;
		
		//limpiar lista de ultimos efectos
		ultimosEfectos.clear();
		
		// obtener el primero y aplicar las medidas.  
		// si puede seguir, aplicar el segundo.
		for(IEfectoObtenible efecto : listaModificadores) {
			if(efecto != null) {
				ultimosEfectos.add(efecto);
				Modificador modif = efecto.obtenerModificador(this);
				//
				if(modif.cambioDeVehiculo) {
					Vehiculo nuevoVehiculo = (Vehiculo) this.miJugador.getVehiculo().vehiculoDeCambio();
					this.miJugador.setVehiculo(nuevoVehiculo);
				}
				
				//efecto.adicionDeMovimientos;
				miJugador.sumaDeMovimientos(modif.adicionDeMovimientos);
				//efecto.productoDeMovimientos
				miJugador.productoDeMovimientos(modif.productoDeMovimientos);
							
				// Si el movimiento no es exitoso, no seguir intentando
				if(!modif.movimientoExitoso) {
					movimientoExitoso = false;
					break;
				}
			}
		}
		
		// Siempre suma 1 por el "turno usado".
		miJugador.sumaDeMovimientos(1.0);
		return movimientoExitoso;
	}

	public Jugador getJugador() {
		
		return miJugador;
	}

	public Dificultad getDificultad() {
		
		return getMapa().getDificultad();
	}
	
	public void setDificultad(Dificultad set) {
		getMapa().setDificultad(set);
	}

	public ArrayList<IEfectoObtenible> getUltimosEfectos() {
		return ultimosEfectos;
	}

	public void setUltimosEfectos(ArrayList<IEfectoObtenible> ultimosEfectos) {
		this.ultimosEfectos = ultimosEfectos;
	}
}
