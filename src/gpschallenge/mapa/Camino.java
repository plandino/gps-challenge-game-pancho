package gpschallenge.mapa;

import gpschallenge.modificador.IEfectoObtenible;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Camino { 
	
    private Par<Cruce> par;
    private ArrayList<IEfectoObtenible> efectos;
    private boolean visible;

    /**
     * Crea el camino entre los 2 cruces a los que conecta.
     * @param c1
     * @param c2
     * @exception Puede lanzar una excepcion si los cruces son iguales.
     */
    public Camino(Cruce c1, Cruce c2) {
		this.par = new Par<Cruce>(c1, c2);
		this.efectos = new ArrayList<IEfectoObtenible>();
    }
    
    /**
     * Agrega un efecto al Camino.
     * @param efecto
     */
       
    public void agregarEfecto(IEfectoObtenible efecto) {
    	efectos.add(efecto);
    }
    
    /**
     * Elimina un efecto presente en el Camino.
     * @param efecto
     */
    public void eliminarEfecto(IEfectoObtenible efecto) {
    	efectos.remove(efecto);
    }
    
    /** Dado un cruce C como inicio, devuelve el cruce al cual se dirige el camino.
     * 
     * @param c
     * @return
     * @throws NoSuchElementException si el camino no contiene al cruce indicado.
     */
    public Cruce getDestino(Cruce c) {
    	return this.par.getOpuesto(c);
    } 
    /**
     * Se fija si el Camino contiene a un cruce determinado.
     * @param c
     * @return {@code true} si el cruce lo contiene, {@code false} si no lo contiene.
     */
    public boolean contieneA(Cruce c) {
    	return this.par.contieneA(c);
    }
    
    /**
     * Devuelve un objeto tipo Par con los dos cruces que este camino conecta.
     * @return Los cruces que conecta el Camino.
     */
    public Par<Cruce> getCruces() {
    	return this.par;
    }
    
    @Override
    public boolean equals(Object c2) {
    	if (c2 == this) return true;
    	if (!(c2 instanceof Camino)) return false;
    	Camino otroCamino = (Camino)c2;
    	Cruce lhsOtro = otroCamino.getCruces().getLeft();
    	Cruce rhsOtro = otroCamino.getCruces().getRight();
    	Cruce lhs = this.par.getLeft();
    	Cruce rhs = this.par.getRight();
    	if (lhsOtro.equals(lhs) && rhsOtro.equals(rhs)) return true;  // igual
    	if (rhsOtro.equals(lhs) && lhsOtro.equals(rhs)) return true;  // invertido
    	return false;
    }
	
    /**
     * Dado un cruce, devuelve la lista de Efectos, en el orden que se deban aplicar, segun la direccion
     * desde la cual se recorre el camino
     * @param cruceActual el Cruce desde el cual se recorre el camino
     * @return Los efectos en el orden adecuado
     * @throws NoSuchElementException si el cruce indicado como parametro no esta contenido en el camino.
     */
	public ArrayList<IEfectoObtenible> getEfectosDesde(Cruce cruceActual) {
		if (!this.contieneA(cruceActual)) {
			throw new NoSuchElementException("Cruce no contenido");
		} else {
			if ((this.par.getLeft()).equals(cruceActual)){
				return this.efectos;
			}
			if ((this.par.getRight()).equals(cruceActual)){
				
				return devolverInverso(efectos);
			}
		}
		return null; //Nunca llega a esta linea, exigida por el compilador.
	}
	
	public void setVisible(boolean vaASerVisible){
		this.visible = vaASerVisible;
	}
	
	public boolean estaVisible(){
		return this.visible;
	}
	
	private ArrayList<IEfectoObtenible> devolverInverso(ArrayList<IEfectoObtenible> efectos) { 
		ArrayList<IEfectoObtenible> copia = new ArrayList<IEfectoObtenible>(); 
		for (int i = efectos.size() - 1 ; i >=0 ; --i){
			copia.add(efectos.get(i));
		}
		return copia; 
	}
	/**
	 * Devuelve los efectos en el orden en que se encuentran, independientemente de la direccion de la cual se recorre el Camino.
	 * @return Los efectos presentes en el Camino.
	 */
	public List<IEfectoObtenible> getEfectosEnOrden() {

		return efectos;
	}
}