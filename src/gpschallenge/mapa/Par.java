package gpschallenge.mapa;

import java.util.NoSuchElementException;

/**
 * Una clase usada como contenedor de dos objetos del mismo tipo. 
 * @author Santiago
 *
 * @param <TTT> La clase de objetos que contendra el Par.
 */
public class Par<TTT> {
	private final TTT l;
	private final TTT r;
	
	/**
	 * Se crea un par con los dos objetos especificados. El orden es importante y permanece inalterado.
	 * @param l 
	 * @param r
	 */
    Par(TTT l, TTT r) {
    	if (l.equals(r)) { throw new IllegalArgumentException("ParIncorrecto"); }
		this.l = l;
		this.r = r;
	}
    
    /**
     * Devuelve el valor booleano correspondiente a la presencia del elemento indicado en el par. 
     * @param elemento
     * @return
     */
	public boolean contieneA(TTT elemento) {
		if (this.l == elemento ||  this.r == elemento) return true;
		if (this.l.equals(elemento) || this.r.equals(elemento)) return true;
		
		return false;
	}
	
	/**
	 * Dado un elemento A, si este se encuentra presente en el par, devuelve el otro objeto contenido en el par.
	 * @param elemento
	 * @return El otro elemento presente en el par.
	 * @throws NoSuchElementException Si el elemento ingresado como parametro no se encuentra en este par.
	 */
	public TTT getOpuesto(TTT elemento) {
		if (this.contieneA(elemento) == true) {
			if (this.l.equals(elemento)) { 
				return this.r; }
			if (this.r.equals(elemento)) {
				return this.l;
		}}
		throw new NoSuchElementException("ElementoInexistente"); 
	}
	
	/**
	 * Devuelve el elemento a la izquierda, es decir el primero de los ingresados como parametro al construir el par.
	 * @return
	 */
	public TTT getLeft() {
		return this.l;
	}
	
	/**
	 * Devuelve el elemento a la derecha, es decir el segundo de los ingresados como parametro al construir el par
	 * @return
	 */
	public TTT getRight() {
		return this.r;
	}
	
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if (obj == this)
			return true;
		if (!(obj instanceof Par))
			return false;
		
		Par<?> otroPar = (Par<?>)obj;
		if (!otroPar.getLeft().equals(this.getLeft())) return false;
		if (!otroPar.getRight().equals(this.getRight())) return false;		 
		return true;
	}
}