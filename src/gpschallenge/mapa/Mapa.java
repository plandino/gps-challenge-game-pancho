package gpschallenge.mapa;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;

import gpschallenge.Jugador;
import gpschallenge.dificultad.Dificultad;
import gpschallenge.mapa.Camino;
import gpschallenge.mapa.Cruce.*;
import gpschallenge.modificador.IEfectoObtenible;
import gpschallenge.modificador.obstaculo.ControlPolicial;
import gpschallenge.modificador.obstaculo.Piquete;
import gpschallenge.modificador.obstaculo.Pozo;
import gpschallenge.modificador.sorpresa.SorpresaCambioDeVehiculo;
import gpschallenge.modificador.sorpresa.SorpresaDesfavorable;
import gpschallenge.modificador.sorpresa.SorpresaFavorable;

import org.w3c.dom.NodeList;
import org.w3c.dom.Element;


/**
 * El mapa sobre el que se desarrolla el juego, conformado por cruces conectados por caminos.
 * @author Santiago
 *
 */
public class Mapa{
		
	private ArrayList<Cruce> cruces;
	private Cruce posicionVehiculo;
	private int crucesAlto;
	private int crucesAncho;
	private Jugador jugador;
	private List<Camino> caminos;
	private Cruce posicionMeta;
	private Dificultad dificultad;
	
	public Mapa() {
		cruces = new ArrayList<Cruce>();
		caminos = new ArrayList<Camino>();
		jugador = new Jugador();
	}
	/**
	 * Construye un mapa con un tamanio inicial.
	 * @param alto 
	 * @param ancho Es el tamanio, medido en cuadras, del mapa.
	 */
	public Mapa(int alto, int ancho) {
		this.cruces = new ArrayList<Cruce>();
		this.caminos = new ArrayList<Camino>();
		this.crucesAncho = ancho + 1;
		this.crucesAlto = alto + 1;
	}
	
	/**
	 * Crea los cruces necesarios para el mapa.
	 */
	public void inicializarCruces() {
		int cantidadCruces = crucesAncho * crucesAlto;
		for (int i=0; i<cantidadCruces; i++) {
			Cruce cruceACrear = new Cruce(i);
			cruces.add(cruceACrear); 
		}
	}
	/**
	 * Devuelve el numero de cruces a lo ancho del mapa.
	 * @return
	 */
	public int getAncho() {
		return this.crucesAncho;
	}
	/**
	 * Devuelve el numero de cruces a lo alto del mapa.
	 * @return
	 */
	public int getAlto() {
		return this.crucesAlto;
	}
	/**
	 * Devuelve la lista de todos los cruces presentes en el mapa.	
	 * @return un ArrayList<Cruce> con los cruces del mapa.
	 */
	public ArrayList<Cruce> getCruces() {
		return this.cruces;
	}
	/**
	 * Devuelve el nombre del jugador al que le corresponde el mapa.
	 * @return
	 */
	public String getNombreJugador() {
		return this.jugador.getNombre();
	}
	
	/**
	 * Fija el nombre del jugador.
	 * @param nombre el nombre a usar.
	 */
	public void setNombreJugador(String nombre) {
		this.jugador.setNombre(nombre);
	}
	
	/**
	 * Devuelve una lista de todos los caminos que conectan cruces en el mapa.
	 * @return una List<Camino> con los caminos presentes en el mapa.
	 */
	public List<Camino> getCaminos() {
		return this.caminos;
	}
	/**
	 * Devuelve el Cruce donde se encuentra el vehiculo.
	 * @return
	 */
	public Cruce getPosicionVehiculo() {
		return posicionVehiculo;
	}
	
	/**
	 * Fija la posicion del vehiculo en un cruce determinado.
	 * @param posicionVehiculo El cruce donde se quiere posicionar el vehiculo.
	 */
	public void setPosicionVehiculo(Cruce posicionVehiculo) {
		this.posicionVehiculo = posicionVehiculo;
	}
	
	/**
	 * Dado un nodo de XML (DOM), el mapa se construye a partir del mismo, tomando todas sus propiedades.
	 * @param nodoMapa el nodo XML desde el cual el mapa tomara sus propiedades.
	 */
	public void cargarMapa(Element nodoMapa) {
		cargarJugador(nodoMapa);
		cargarDimensiones(nodoMapa);
		
		String dificultadLeida = nodoMapa.getElementsByTagName("dificultad").item(0).getTextContent();
		this.setDificultad(Dificultad.DificultadAPartirDeString(dificultadLeida));
		
		this.setLimiteMovimientos(Double.parseDouble(nodoMapa.getElementsByTagName("limiteMovimientos").item(0).getTextContent()));
		
		cargarCruces(nodoMapa);
		
		cargarCaminos(nodoMapa);
		
		//incializar el vehiculo en alguna posicion
		inicializarPosicionesAleatorias();
	}
	
	/**
	 * Carga la meta y la posicion actual desde un nodo XML. Se hace aparte para diferenciar partida nueva
	 * De partida ya hecha para retomar.
	 * @param nodoMapa el nodo del cual se cargaran los datos.
	 */
	public void cargarDatosPartidaGuardada(Element nodoMapa) {
		//Carga de la posicion actual:
		int identificadorDePosicion = Integer.parseInt(nodoMapa.getElementsByTagName("posicion").item(0).getTextContent());
		Cruce cruceABuscar = new Cruce(identificadorDePosicion);
		int posicionCruceBuscado = getCruces().indexOf(cruceABuscar);
		if (posicionCruceBuscado == -1) System.out.println("Se ha intentado cargar cruce incorrecto para la posicion");
		else { this.setPosicionVehiculo(this.getCruces().get(posicionCruceBuscado)); }
		
		//Carga de la meta
		int identificadorDeMeta = Integer.parseInt(nodoMapa.getElementsByTagName("meta").item(0).getTextContent());
		Cruce cruceMeta = new Cruce(identificadorDeMeta);
		int posicionCruceMeta = getCruces().indexOf(cruceMeta);
		if (posicionCruceBuscado == -1) System.out.println("Se ha intentado cargar un cruce incorrecto para la meta");
		else { this.setPosicionMeta(this.getCruces().get(posicionCruceMeta)); }
		
		//Carga de los movimientos
		double movimientosActuales = Double.parseDouble((nodoMapa.getElementsByTagName("movimientos").item(0).getTextContent()));
		this.jugador.getContador().setCantidad(movimientosActuales);
	}
	
	/**
	 * Genera un efecto a partir de un string determinado. Usado en la carga de mapa.
	 * @param efecto el nombre de la clase del Efecto a crear.
	 * @return una instancia del Efecto cuya clase corresponde al String ingresado
	 */
	private IEfectoObtenible EfectoAPartirDeString(String efecto) {
		if(efecto.equals("SorpresaFavorable")) return new SorpresaFavorable();
		if(efecto.equals("SorpresaDesfavorable")) return new SorpresaDesfavorable();
		if(efecto.equals("SorpresaCambioDeVehiculo")) return new SorpresaCambioDeVehiculo();
		if(efecto.equals("Pozo")) return new Pozo();
		if(efecto.equals("Piquete")) return new Piquete();
		if(efecto.equals("ControlPolicial")) return new ControlPolicial();
		return null;
	}
	/**
	 * Elige los cruces que van a contener al vehiculo, inicialmente, y a la meta. 
	 * Las posiciones de ambos no terminan siendo iguales, para evitar comportamiento indeseado.
	 */
	public void inicializarPosicionesAleatorias() {
		int size = getCruces().size();
		assert(size > 1);  // crash early, crash hard.
		
		Random random = new Random();
		Cruce posLargada = getCruces().get(random.nextInt(size)); 
		Cruce posMeta;
		do {
			posMeta = getCruces().get(random.nextInt(size)); 
		} while(posMeta.equals(posLargada));
		
		setPosicionVehiculo(posLargada);
		setPosicionMeta(posMeta);
	}
	
	/**
	 * Fija un cruce que contenga la meta.
	 * @param posMeta
	 */
	public void setPosicionMeta(Cruce posMeta) {
		posicionMeta = posMeta;
	}
	
	/**
	 * Devuelve el cruce que contiene a la meta.
	 * @return
	 */
	public Cruce getPosicionMeta() {
		return posicionMeta;
	}
	/**
	 * Devuelve la dificultad correspondiente al mapa.
	 * @return
	 */
	public Dificultad getDificultad() {
		return dificultad;
	}
	/**
	 * Fija una dificultad para el mapa.
	 * @param dificultad
	 */
	public void setDificultad(Dificultad dificultad) {
		this.dificultad = dificultad;
	}

	/**
	 * Devuelve el numero de movimientos limite para ganar el mapa.
	 * @return
	 */

	public double getLimiteMovimientos() {
		return jugador.getContador().getLimiteDeMovimientos();
	}
	/**
	 * Fija el numero de movimientos limite para ganar el mapa.
	 * @param limiteMovimientos
	 */
	public void setLimiteMovimientos(double limiteMovimientos) {
		this.jugador.getContador().setLimiteDeMovimientos(limiteMovimientos);
	}
	
	/**
	 * Ordena una lista de EfectosObtenibles, situando primero las sorpresas y luego los obstaculos.
	 * @param efectos la lista de EfectosObtenibles a ordenar.
	 */
	private void ordenarSegunCorresponde(List<IEfectoObtenible> efectos) {
		IEfectoObtenible primero = null;
		IEfectoObtenible segundo = null;
		for (IEfectoObtenible efecto : efectos) {
			if (efecto.getClass().equals(Piquete.class)) segundo = efecto;
			if (efecto.getClass().equals(Pozo.class)) segundo = efecto;
			if (efecto.getClass().equals(ControlPolicial.class)) segundo = efecto;
			
			if (efecto.getClass().equals(SorpresaFavorable.class)) primero = efecto;
			if (efecto.getClass().equals(SorpresaDesfavorable.class)) primero = efecto;
			if (efecto.getClass().equals(SorpresaCambioDeVehiculo.class)) primero = efecto;
		}
		efectos.clear();
		efectos.add(primero);
		efectos.add(segundo);
	}
	
	/**
	 * Pre: ninguna.
	 * Post: todos los caminos en el mapa pasan a tener visibilidad igual a false
	 */
	public void setFalseAVisibilidadEnTodosLosCaminos(){
		List<Camino> listaCaminos = this.getCaminos();
		for (Camino camino : listaCaminos){
			camino.setVisible(false);
		}
	}
	
	/**
	 * 
	 * Pre: ninguna.
	 * Post: Todos los caminos a una distancia {@code topeVisibilidad} del cruce que pase, van a tener visibilidad true, todos los demas van a quedar con visibilidad false
	 * 
	 * @param cruce: El cruce central a partir del cual quiero que se esparza la visibilidad
	 * @param topeVisibilidad: Cantidad de caminos a la redonda que van a estar visibles
	 */
	public void setCaminosVisibleCercanosAlVehiculo(Cruce cruce, int topeVisibilidad){
		topeVisibilidad = topeVisibilidad - 1;
		for (Entry<Direccion, Camino> entry : cruce.getCaminosSalientes().entrySet()){
			Camino camino = entry.getValue();
			camino.setVisible(true);
			if (topeVisibilidad > 0){
				Cruce cruceSiguiente = camino.getDestino(cruce);
				setCaminosVisibleCercanosAlVehiculo(cruceSiguiente, topeVisibilidad);
			}
		}
	}
	public Jugador getJugador() {
		return jugador;
	}
	public void setJugador(Jugador jug) {
		this.jugador = jug;
	}
	
	//Metodos usados para la carga del mapa desde un nodo DOM:
	
	private void cargarCaminos(Element nodoMapa) {
		NodeList listaCaminos = nodoMapa.getElementsByTagName("camino");
		for (int i=0; i<listaCaminos.getLength(); ++i) {
			Element nCamino = (Element)listaCaminos.item(i);
			Element nCruceA = (Element)nCamino.getElementsByTagName("cruceA").item(0);
			String identificadorA = nCruceA.getElementsByTagName("id").item(0).getTextContent();
			String direccionDesdeA = nCamino.getElementsByTagName("nombreDesdeA").item(0).getTextContent();
			Element nCruceB = (Element)nCamino.getElementsByTagName("cruceB").item(0);
			String identificadorB = nCruceB.getElementsByTagName("id").item(0).getTextContent();
			String direccionDesdeB = nCamino.getElementsByTagName("nombreDesdeB").item(0).getTextContent();
			int idA = Integer.parseInt(identificadorA);
			int idB = Integer.parseInt(identificadorB);
			Cruce cruceA = null;
			Cruce cruceB = null;
			for(Cruce cruce : cruces) {
				if(cruce.getIdentificador() == idA) {
					cruceA = cruce;
				}
				if(cruce.getIdentificador() == idB) {
					cruceB = cruce;
				}
			}
			if(cruceA == null || cruceB == null) {
				System.out.println("error!! no encontró cruce para camino.");
				System.exit(1);
			}
			Camino caminoACrear = new Camino(cruceA, cruceB);
			
			//Carga de Sorpresas y obstaculos
			if (nCamino.getElementsByTagName("efecto").getLength() != 0) {
				NodeList listaEfectos = nCamino.getElementsByTagName("efecto");
				for (int j = 0; j<listaEfectos.getLength(); ++j) {
					String nombreEfecto = listaEfectos.item(j).getTextContent();
					IEfectoObtenible efecto = EfectoAPartirDeString(nombreEfecto);
					if (efecto == null) System.out.println("Error!");
					caminoACrear.agregarEfecto(efecto);
				}
			}
			
			ordenarSegunCorresponde(caminoACrear.getEfectosEnOrden()); //Ordena los efectos en el camino.
			
			
			this.caminos.add(caminoACrear);
			if (direccionDesdeA != null && !direccionDesdeA.isEmpty())
			cruces.get(cruces.indexOf(cruceA)).agregarCamino(caminoACrear, Direccion.valueOf((direccionDesdeA).toUpperCase()));
			if (direccionDesdeB != null && !direccionDesdeB.isEmpty())
			cruces.get(cruces.indexOf(cruceB)).agregarCamino(caminoACrear, Direccion.valueOf((direccionDesdeB).toUpperCase()));
		}
	}
	private void cargarCruces(Element nodoMapa) {
		NodeList listaCruces = nodoMapa.getElementsByTagName("cruce");
		for (int i = 0; i<listaCruces.getLength(); ++i) {
			Element nCruce = (Element)listaCruces.item(i);
			Element nIdentificador = (Element)nCruce.getElementsByTagName("identificador").item(0);
			int id = Integer.parseInt(nIdentificador.getTextContent()); //El identificador unico del Cruce.
			Cruce cruceACrear = new Cruce(id);
			this.cruces.add(cruceACrear);
		}
	}
	private void cargarDimensiones(Element nodoMapa) {
		Element nodoDimensiones = (Element)nodoMapa.getElementsByTagName("dimensiones").item(0);
		this.crucesAncho = Integer.parseInt(nodoDimensiones.getElementsByTagName("ancho").item(0).getTextContent()) + 1;
		this.crucesAlto = Integer.parseInt(nodoDimensiones.getElementsByTagName("alto").item(0).getTextContent()) +1;
	}
	private void cargarJugador(Element nodoMapa) {
		String nombre = nodoMapa.getElementsByTagName("jugador").item(0).getTextContent();
		if (nombre == null || nombre.isEmpty()) this.jugador.setNombre("");
		else this.jugador.setNombre(nombre);
	}

}
