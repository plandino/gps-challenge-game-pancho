package gpschallenge.ranking;

public class Puntaje {
	private double puntaje;
	private String nombre;
	
	/**
	 * Se construye un puntaje, que contiene un nombre y la puntuacion que le corresponde
	 * @param unNombre El nombre correspondiente
	 * @param unPuntaje La cantidad de puntos
	 */
	public Puntaje(String unNombre, double unPuntaje){
		this.setearPuntaje(unPuntaje);
		this.setearNombre(unNombre);
	}
	
	/**
	 * Fija un nombre al puntaje.
	 * @param unNombre
	 */
	public void setearNombre(String unNombre) {
		nombre = unNombre;
		
	}
	/**
	 * Fija una puntacion al nombre.
	 * @param unPuntaje
	 */
	public void setearPuntaje(double unPuntaje) {
		puntaje = unPuntaje;
		
	}
	
	public double getPuntaje(){
		return puntaje;
	}
	
	public String getNombre(){
		return nombre;
	}
}
