package gpschallenge.ranking;

import java.io.*;
import java.util.ArrayList;

import com.csvreader.CsvWriter;
import com.csvreader.CsvReader;

public class ManejadorDeArchivosCSV {
	
	private String miRuta;
	
	// Cada ManejadorDeArchivos funciona con 1 archivo, si quiero usar mas de 1 archivo, hay que crear una instancia por cada archivo
	public ManejadorDeArchivosCSV(String rutaDelArchivo){
		this.setearRutaDelArchivo(rutaDelArchivo);
	}

	private void setearRutaDelArchivo(String rutaDelArchivo) {
		this.miRuta = rutaDelArchivo;
	}
	
	/**
	 * 
	 * @return: La lista de puntajes obtenidos del archivo, si el archivo no existe, lo crea y devulve un ArrayList<Puntaje> vacio 
	 */
	public ArrayList<Puntaje> obtenerTodoElArchivo(){
		ArrayList<Puntaje> unArrayADevolver = new ArrayList<Puntaje>();
		CsvReader lectorCsv = null;
		try {
			// Si el archivo no existe, devulve un ArrayList<Puntaje> vacio y crea el archivo en la ubicacion dada
			boolean yaExiste = new File(miRuta).exists();
			if(!yaExiste){
				crearArchivo();
	           	return unArrayADevolver; 
			}
			
	        lectorCsv = new CsvReader(this.miRuta);
	        lectorCsv.readHeaders();

	        while (lectorCsv.readRecord())
	        {
	            String nombre = lectorCsv.get(0);
	            double puntaje = Double.parseDouble(lectorCsv.get(1));
	             
	            unArrayADevolver.add(new Puntaje(nombre,puntaje));    
	        }
	        
	        lectorCsv.close();
	        
		} catch (IOException e) {
	            e.printStackTrace();
	    }
		return unArrayADevolver;
		
	}
	
	/**
	 * Escribe en el archivo la lista de puntajes que me pasan por parametro
	 * @param unArrayDePuntajes: Lista de puntajes a escribir en el archivo
	 */
	public void escribirEnArchivo(ArrayList<Puntaje> unArrayDePuntajes){
		CsvWriter escritorCsv = null;
        try {
        	// De esta forma cada vez que voy a escribir el archivo, lo vacio, le creo las cabeceras y escribo lo que quiero a continuacion
        	this.crearArchivo();
            escritorCsv = new CsvWriter(new FileWriter(miRuta, true), ',');
            // Forma de iterar en un ArrayList de Puntajes [ArrayList<Puntaje> unArrayDePuntajes]         
            for(Puntaje unPuntaje : unArrayDePuntajes ){
            	// Verifico si el archivo ya existia, meto un salto de linea para que comience a escribir en la linea de abajo
            	escritorCsv.write(unPuntaje.getNombre());
            	// Tengo que transformar el int de puntaje a un String
            	escritorCsv.write(Double.toString(unPuntaje.getPuntaje()));
            	escritorCsv.endRecord();                   
            }
             
            escritorCsv.close();
         
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        	escritorCsv.close();
        }
    }
	
	/**
	 * Crea el archivo donde se van a guardar los puntajes que me pasen, junto con sus cabeceras de Nombre y Puntaje
	 */
	public void crearArchivo(){
		CsvWriter escritorCsv = null;
		try {
            // Crea el escritor para archivos CSV. Al poner false por parametro, sobreescribe el archivo. Tambien le introduce las cabeceras
            escritorCsv = new CsvWriter(new FileWriter(miRuta, false), ',');
           	escritorCsv.write("Nombre");
           	escritorCsv.write("Puntaje");
           	escritorCsv.endRecord(); 
            escritorCsv.close();
         
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        	escritorCsv.close();
        }
	}	
}
