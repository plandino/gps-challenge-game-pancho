package gpschallenge.ui.graphical;

import gpschallenge.ui.graphical.vistas.VentanaManager;

public class FramePrincipal {
	
	public FramePrincipal(){
		@SuppressWarnings("unused")
		VentanaManager vista  = new VentanaManager();
	}
		
    public static void main(String[] args) {
    	new FramePrincipal();
    }

}