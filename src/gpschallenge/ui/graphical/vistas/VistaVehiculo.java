package gpschallenge.ui.graphical.vistas;

import gpschallenge.mapa.Cruce;
import gpschallenge.mapa.Mapa;
import gpschallenge.vehiculo.Auto;
import gpschallenge.vehiculo.Camioneta4x4;
import gpschallenge.vehiculo.Moto;
import gpschallenge.vehiculo.Vehiculo;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;

@SuppressWarnings("serial")
public class VistaVehiculo extends Vista{
	
	private Vehiculo miVehiculo;
		
	public VistaVehiculo(Mapa mapa, Vehiculo vehiculo){
		super(mapa);
		setVehiculo(vehiculo);
	}
	
	private void setVehiculo(Vehiculo unVehiculo) {
		miVehiculo = unVehiculo;
	}
	
	public void dibujarse(Graphics vehiculo){
		// Guardo la posX y posY de donde se encuentra el cruce, donde esta el vehiculo, para poder
	    // dibujarlo en el Canvas
		Cruce cruceVehiculo = miMapa.getPosicionVehiculo();
		guardarPosicionDelCruceParaElCanvas(cruceVehiculo);
		
		Image imagen = null;
		// Dependiendo de que instancia de vehiculo sea, dibuja el vehiculo con una imagen diferente
		if(miVehiculo.getClass().equals(Auto.class)){
			imagen = new ImageIcon(getClass().getResource("/gpschallenge/recursos/imagenes/imagenAuto.png")).getImage();
		}
		
		if(miVehiculo.getClass().equals(Moto.class)){
			imagen = new ImageIcon(getClass().getResource("/gpschallenge/recursos/imagenes/imagenMoto.png")).getImage();
		}
		
		if(miVehiculo.getClass().equals(Camioneta4x4.class)){
			imagen = new ImageIcon(getClass().getResource("/gpschallenge/recursos/imagenes/imagenCamioneta.png")).getImage();			
		} 		
		// El tamanioCalle se usar para darle el tamanio del vehiculo, ya que tiene que terner las mismas dimensiones que la calle
		vehiculo.drawImage(imagen, posX, posY, TAMANIO_CALLE , TAMANIO_CALLE,  null);
		
	}

}
