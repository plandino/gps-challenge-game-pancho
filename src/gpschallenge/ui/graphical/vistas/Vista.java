package gpschallenge.ui.graphical.vistas;

import gpschallenge.mapa.Cruce;
import gpschallenge.mapa.Mapa;

import java.awt.Canvas;
import java.awt.Graphics;

@SuppressWarnings("serial")
public abstract class Vista extends Canvas{
	
	// Todas las vistas usan estos parametros
	protected int TAMANIO_MANZANA = 50;
	protected int TAMANIO_CALLE = 20;
	protected Mapa miMapa;
	protected int visibilidad = 2;
	protected int posX;
	protected int posY;
	
	public abstract void dibujarse(Graphics graficoADibujar);	
	
	public Vista(Mapa mapa){
		setMapa(mapa);
	}
	
	public void setMapa(Mapa mapa) {
		miMapa = mapa;	
	}
	
	/**
	 * Obtiene la posicion X e Y, en el Canvas, del cruce que le paso por parametro, esta 
	 * posicion se puede usar para dibujar en esa posicion exacta
	 * @param cruce: Cruce del cual quiero obtener las coordenadas X e Y en el Canvas
	 */
	protected void guardarPosicionDelCruceParaElCanvas(Cruce cruce){
	   	/* Tengo que obtener el identificador del cruce donde esta el vehiculo.
	   	 * Luego obtengo el resto de la division entre el indice y  
	   	 * la cantidad de cruces en el ancho para obtener la posicion X del vehiculo.
	   	 * Despues divido el indice por el ancho de cruces, para obtener la posicon Y del vehiculo
	   	 * La posicion X e Y se refieren al numero de cruce en el que se encuentra el vehiculo.
	   	 */
		int posRelativa = cruce.getIdentificador();
		// Tengo que dividir en base a la cantidad de cruces. getAncho() de un Mapa me da el ancho en cruces
		// Esto me da las coordenadas del cruce en el mapa
	   	posX = (posRelativa) % (miMapa.getAncho());
	   	posY = posRelativa / (miMapa.getAncho());
	   	// Multiplicando estas coordenadas por la ubicacion de un Cruce en el mapa, obtengo
	   	// las coordenadas, del vehiculo en el Mapa, en el Canvas.
	   	int posCruce = TAMANIO_CALLE + TAMANIO_MANZANA;
	   	posX = posX*posCruce;
	   	posY = posY*posCruce;
	 }
}
