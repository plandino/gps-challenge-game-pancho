package gpschallenge.ui.graphical.vistas;

import gpschallenge.mapa.Camino;
import gpschallenge.mapa.Cruce;
import gpschallenge.mapa.Mapa;
import gpschallenge.mapa.Par;
import gpschallenge.modificador.IEfectoObtenible;
import gpschallenge.modificador.obstaculo.ControlPolicial;
import gpschallenge.modificador.obstaculo.Piquete;
import gpschallenge.modificador.obstaculo.Pozo;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.List;
import javax.swing.ImageIcon;

@SuppressWarnings("serial")
public class VistaCaminosVisibles extends Vista{
	
	private Image imagen;
	// Estos dos atributos se usan para hacer que la Sorpresa se dibuje en la parte izq o superior
	// de un camino y el Obstaculo en la parte derecha o inferior
	private int UBICACION_SORPRESA = 8;
	private int UBICACION_OBSTACULO = 2;
	
	public VistaCaminosVisibles(Mapa unMapa){
		super(unMapa);

	}
		
	/**
	 * Esto dibuja los caminos visibles, junto con las sorpresas y obstaculos que tengan
	 * @param grafico: Es un Graphics que se usa para dibujar
	 */
	@Override
	public void dibujarse(Graphics grafico) {
		// Antes de dibujar los caminos, seteo que son todos NO visibles, para luego actualizar los
		// que si estan visibles cerca del vehiculo
		miMapa.setFalseAVisibilidadEnTodosLosCaminos();
		miMapa.setCaminosVisibleCercanosAlVehiculo(miMapa.getPosicionVehiculo(), visibilidad);
	    List<Camino> listaDeCaminos = miMapa.getCaminos();
		int identificador = 0;
		// ESTO DIBUJA LOS OBSTACULOS Y SORPRESAS HORIZONTALES
		for (int i = 0 ; i< (miMapa.getAlto()) ; ++i){
			for ( int j = 0 ; j< (miMapa.getAncho() - 1) ; ++j){
				
				Camino unCamino = listaDeCaminos.get(identificador);
				if (unCamino.estaVisible()){			
					
					if(caminoEsHorizontal(unCamino)){
						dibujarCaminoHorizontal(grafico,j,i);			// Como estoy en un camino visible, lo dibujo		
						
						List<IEfectoObtenible> listaEfectos = unCamino.getEfectosEnOrden();
						
						if (listaEfectos.get(0) != null){
							int posX = TAMANIO_CALLE + ((TAMANIO_CALLE + TAMANIO_MANZANA)* j) + (TAMANIO_MANZANA/UBICACION_SORPRESA);
							int posY = i * (TAMANIO_CALLE + TAMANIO_MANZANA);
							imagen = new ImageIcon(getClass().getResource("/gpschallenge/recursos/imagenes/sorpresa.png")).getImage();
							dibujarEfecto(grafico, posX, posY, TAMANIO_CALLE);
						}
						
						if(listaEfectos.get(1) != null) {	
							if(listaEfectos.get(1) instanceof Piquete){
								imagen = new ImageIcon(getClass().getResource("/gpschallenge/recursos/imagenes/piquete.png")).getImage();
							}
						
							if(listaEfectos.get(1) instanceof ControlPolicial){
								imagen = new ImageIcon(getClass().getResource("/gpschallenge/recursos/imagenes/policia.png")).getImage();
							}
						
							if(listaEfectos.get(1) instanceof Pozo){
								imagen = new ImageIcon(getClass().getResource("/gpschallenge/recursos/imagenes/pozo.png")).getImage();
							
							}
							int posX = TAMANIO_CALLE + ((TAMANIO_CALLE + TAMANIO_MANZANA)* j) + (TAMANIO_MANZANA/UBICACION_OBSTACULO);
							int posY = i * (TAMANIO_CALLE + TAMANIO_MANZANA);
							dibujarEfecto(grafico, posX, posY, TAMANIO_CALLE);								
						}
				}
				
				}
				identificador = identificador + 1;
			}
			
		}

		// ESTO DIBUJA OBSTACULOS Y SORPRESAS VERTICALES
		for (int i = 0 ; i< (miMapa.getAncho()) ; ++i){
			for ( int j = 0 ; j< (miMapa.getAlto() - 1) ; ++j){
				Camino unCamino = listaDeCaminos.get(identificador);
				
				if(unCamino.estaVisible()){
					dibujarCaminoVertical(grafico, i, j); 	// Como estoy en un camino visible lo dibujo
					List<IEfectoObtenible> listaEfectos = unCamino.getEfectosEnOrden();
					
					if (listaEfectos.get(0) != null){
						int posX = i * (TAMANIO_CALLE + TAMANIO_MANZANA);
						int posY = TAMANIO_CALLE + ((TAMANIO_CALLE + TAMANIO_MANZANA)* j) + (TAMANIO_MANZANA/UBICACION_SORPRESA);
						imagen = new ImageIcon(getClass().getResource("/gpschallenge/recursos/imagenes/sorpresa.png")).getImage();
						dibujarEfecto(grafico, posX, posY, TAMANIO_CALLE);
					}
					
					if(listaEfectos.get(1) != null) {
						if(listaEfectos.get(1) instanceof Piquete){
							imagen = new ImageIcon(getClass().getResource("/gpschallenge/recursos/imagenes/piquete.png")).getImage();
						}
					
						if(listaEfectos.get(1) instanceof ControlPolicial){
							imagen = new ImageIcon(getClass().getResource("/gpschallenge/recursos/imagenes/policia.png")).getImage();
						}
					
						if(listaEfectos.get(1) instanceof Pozo){
							imagen = new ImageIcon(getClass().getResource("/gpschallenge/recursos/imagenes/pozo.png")).getImage();
						
						}
						int posX = i * (TAMANIO_CALLE + TAMANIO_MANZANA);
						int posY = TAMANIO_CALLE + ((TAMANIO_CALLE + TAMANIO_MANZANA)* j) + (TAMANIO_MANZANA/UBICACION_OBSTACULO);
						dibujarEfecto(grafico, posX, posY, TAMANIO_CALLE);										
					}
				}
				identificador = identificador + 1;
			}
			
		}
	
    }
	
	/**
	 * Dibuja el camino en forma Horizontal
	 * @param grafico: Graphics que se usa para dibujar en el Canvas
	 * @param x: Coordenada X del camino en el Canvas
	 * @param y: Coordenada Y del camino en el Canvas
	 */
	private void dibujarCaminoHorizontal(Graphics grafico, int x, int y) {
		int tamanioXCamino =  TAMANIO_CALLE + TAMANIO_MANZANA;
		int tamanioYCamino = TAMANIO_CALLE;
		int posXCamino = x *(TAMANIO_CALLE + TAMANIO_MANZANA);
		int posYCamino = y * (TAMANIO_CALLE + TAMANIO_MANZANA);
		grafico.setColor(Color.lightGray);
		grafico.fillRect(posXCamino, posYCamino, tamanioXCamino, tamanioYCamino);
		
	}
	
	/**
	 * Dibuja el camino en forma Vertical
	 * @param grafico: Graphics que se usa para dibujar en el Canvas
	 * @param x: Coordenada X del camino en el Canvas
	 * @param y: Coordenada Y del camino en el Canvas
	 */
	private void dibujarCaminoVertical(Graphics grafico, int x, int y){
		int tamanioXCamino =  TAMANIO_CALLE ;
		int tamanioYCamino = TAMANIO_CALLE + TAMANIO_MANZANA + TAMANIO_CALLE;
		int posXCamino = x *(TAMANIO_CALLE + TAMANIO_MANZANA);
		int posYCamino = y * (TAMANIO_CALLE + TAMANIO_MANZANA);
		grafico.setColor(Color.lightGray);
		grafico.fillRect(posXCamino, posYCamino, tamanioXCamino, tamanioYCamino);
	}
	
	/**
	 * Sirve para saber si estoy en un camino horizontal
	 * @param unCamino: El camino a analizar si es horizontal
	 * @return: True si el camino es horizontal, false si es vertical
	 */
	private boolean caminoEsHorizontal(Camino unCamino) {
		Par<Cruce> crucesPar = unCamino.getCruces();
		Cruce cruceIzq  = crucesPar.getLeft();
		Cruce cruceDerecho = crucesPar.getRight();
		// Si la diferencia entre los indentificadores es igual a 1, estoy en un camino horizontal
		int resto = cruceDerecho.getIdentificador() - cruceIzq.getIdentificador();
		boolean caminoHorizontal = ((resto - 1) == 0);
		return caminoHorizontal;
	}
	
	/**
	 * Dibuja el efecto(Sorpresas y Obstaculos), en la posicion donde se lo indican en el Canvas, con la imagen cargada anteriormente
	 * @param sorpresaADibujar: Graphics que se usa para dibujar en el Canvas
	 * @param posX: Posicion en el eje X a dibujar en el Canvas
	 * @param posY:Posicion en el eje Y a dibujar en el Canvas
	 * @param tamanioCalle: El tamanio con el cual se va a dibujar el efecto, tiene sentido que sea igual al de la calle
	 */
	private void dibujarEfecto(Graphics efectoADibujar, int posX, int posY,int tamanioCalle) {
		// El tamanioCalle se usar para darle el tamanio de la sorpresa, ya que tiene que terner las mismas dimensiones que la calle
		efectoADibujar.drawImage(imagen, posX, posY, tamanioCalle , tamanioCalle,  null);	
	}
	
}
