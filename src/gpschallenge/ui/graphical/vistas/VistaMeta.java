package gpschallenge.ui.graphical.vistas;

import gpschallenge.mapa.Cruce;
import gpschallenge.mapa.Mapa;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;

@SuppressWarnings("serial")
public class VistaMeta extends Vista {

	public VistaMeta(Mapa mapa) {
		super(mapa);
	}
	
	/** 
	 * Dibuja la meta en el Canvas
	 */
	@Override
	public void dibujarse(Graphics graficoADibujar) {
		Cruce cruceDeLaMeta = miMapa.getPosicionMeta();
		guardarPosicionDelCruceParaElCanvas(cruceDeLaMeta);
		dibujarMeta(graficoADibujar);
		
	}
	
	/**
	 * Dibuja la meta en la posicion que tiene que ser guardada anteriormente con guardarPosicionDelCruceParaElCanvas(Cruce cruceDeLaMeta);
	 * @param meta: Graphics usado para dibujar
	 */
	private void dibujarMeta(Graphics meta) {
		Image imagen = null;
		imagen = new ImageIcon(getClass().getResource("/gpschallenge/recursos/imagenes/meta.png")).getImage();
		meta.drawImage(imagen, posX, posY, TAMANIO_CALLE , TAMANIO_CALLE,  null);		
	}

}
