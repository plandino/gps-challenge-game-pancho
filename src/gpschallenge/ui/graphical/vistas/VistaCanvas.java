package gpschallenge.ui.graphical.vistas;

import gpschallenge.mapa.Mapa;
import gpschallenge.vehiculo.Vehiculo;

import java.awt.Graphics;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class VistaCanvas extends Vista {
	
	private ArrayList<Vista> vistas;

	// A VistaCanvas me pasan por argumento el mapa y el vehiculo
	public VistaCanvas(Mapa unMapa, Vehiculo unVehiculo) {
		super(unMapa);
		setVistas(unMapa,unVehiculo);
	}
			   
	/**
	 * Carga todas las vistas que se van a usar para dibujar
	 * @param unMapa: Mapa a dibujar
	 * @param unVehiculo: Vehiculo a dibujar
	 */
    public void setVistas(Mapa unMapa, Vehiculo unVehiculo) {
    	vistas = new ArrayList<Vista>();
		VistaVehiculo vistaVehiculo = new VistaVehiculo(unMapa,unVehiculo);
		// VistaCaminosVisibles tambien dibuja las sorpresas y obstaculos que se encuentran en los caminos visibles
		VistaCaminosVisibles vistaCaminos = new VistaCaminosVisibles(unMapa);
		VistaMeta vistaMeta = new VistaMeta(unMapa);
		vistas.add(vistaCaminos);
		vistas.add(vistaVehiculo);	
		vistas.add(vistaMeta);
		
	}
  
    /**
     * Soobreescribo el paint para poder pintar en el Canvas como yo quiero
     */
    public void paint(Graphics graficoADibujar) {
    	for(Vista unaVista : vistas){
    		unaVista.dibujarse(graficoADibujar);
    	}
    	int anchoCanvas = (miMapa.getAncho() - 1) * (TAMANIO_CALLE + TAMANIO_MANZANA) + TAMANIO_CALLE;
    	int altoCanvas = (miMapa.getAlto() - 1) * (TAMANIO_CALLE + TAMANIO_MANZANA) + TAMANIO_CALLE;
    	this.setSize(anchoCanvas, altoCanvas);
    }
        	
	/**
	 * En VistaCanvas no se dibuja nada, solo llama a otras vistas a dibujarse
	 */
	@Override
	public void dibujarse(Graphics graficoADibujar) {
		// TODO Auto-generated method stub
		
	}
	
}