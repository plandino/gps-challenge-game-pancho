package gpschallenge.ui.graphical.vistas;

import gpschallenge.Jugador;
import gpschallenge.Partida;
import gpschallenge.mapa.Mapa;
import gpschallenge.ranking.Ranking;
import gpschallenge.ui.Controlador;
import gpschallenge.ui.graphical.VistaPartida;
import gpschallenge.ui.graphical.paneles.CustomPanel;
import gpschallenge.ui.graphical.paneles.PanelComenzarPartida;
import gpschallenge.ui.graphical.paneles.PanelGanador;
import gpschallenge.ui.graphical.paneles.PanelInicio;
import gpschallenge.ui.graphical.paneles.PanelMenuJugador;
import gpschallenge.ui.graphical.paneles.PanelPerdedor;
import gpschallenge.ui.graphical.paneles.PanelPuntajes;
import gpschallenge.ui.graphical.paneles.PanelUsuarioNuevo;
import gpschallenge.ui.graphical.paneles.PanelUsuarioViejo;
import gpschallenge.vehiculo.Auto;
import gpschallenge.vehiculo.Camioneta4x4;
import gpschallenge.vehiculo.Moto;
import gpschallenge.vehiculo.Vehiculo;
import gpschallenge.xmlhandler.XMLHandler;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class VentanaManager implements ActionListener, ChangeListener {
	public JFrame ventanaPrincipal;
	CustomPanel panelActual;
	PanelInicio panelInicio;
	PanelUsuarioViejo panelUsuarioViejo;
	PanelUsuarioNuevo panelUsuarioNuevo;
	PanelMenuJugador panelMenuJugador;
	PanelComenzarPartida panelComenzarPartida;
	PanelGanador panelGanador;
	private String nombreJugador;
	//PanelElegirUsuario panelElegirUsuario;
	PanelPuntajes panelPuntajes;
	private Partida partida;
	VistaPartida miVistaPartida;
	PanelPerdedor panelPerdedor;
	private Ranking miRanking;
	
	
	public VentanaManager() {
		setNombreJugador("sin_nombre");
		crearRanking("src/gpschallenge/recursos/ranking/ranking.csv");
		resetVentanaPrincipal();
		agregarPanelInicio();
		actualizarVentana();
	}
	
	public void agregarPanelInicio() {
		// TODO Auto-generated method stub
		panelInicio = new PanelInicio(this);
		agregarPanel(panelInicio);
	}

	private void agregarPanel(CustomPanel panel) {
		ventanaPrincipal.add(panel);
		panelActual = panel;
	}
	
	public void eliminarPanel(CustomPanel panelAEliminar){
		ventanaPrincipal.remove(panelAEliminar);
		actualizarVentana();
	}

	public void agregarPanelUsuarioNuevo() {
		panelUsuarioNuevo = new PanelUsuarioNuevo(this);
		agregarPanel(panelUsuarioNuevo);
	}	
	
	public void agregarPanelUsuarioViejo() {
		panelUsuarioViejo = new PanelUsuarioViejo(this);
		agregarPanel(panelUsuarioViejo);
	}	
	
	public void agregarPanelMenuJugador() {
		panelMenuJugador = new PanelMenuJugador(this);
		agregarPanel(panelMenuJugador);
		
	}

	public void agregarPanelComenzarPartida() {
		panelComenzarPartida = new PanelComenzarPartida(this);
		agregarPanel(panelComenzarPartida);
	}
	
	public void agregarPanelPuntajes(){
		panelPuntajes = new PanelPuntajes(this);
		agregarPanel(panelPuntajes);
	}
	
	public void agregarPanelGanador(){
		panelGanador = new PanelGanador(this);
		agregarPanel(panelGanador);
	}
	
	public void agregarPanelPerdedor() {
		panelPerdedor = new PanelPerdedor(this);
		agregarPanel(panelPerdedor);
	}
	
	public void actualizarVentana() {
		ventanaPrincipal.validate();
		
		ventanaPrincipal.repaint();
		ventanaPrincipal.pack();
	}

	public void resetVentanaPrincipal() {
		ventanaPrincipal = new JFrame();
		ventanaPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		actualizarVentana();
		ventanaPrincipal.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		if(panelActual != null) {	
			panelActual.procesarAccion(event);
		}
		actualizarVentana();
	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
		
	}

	public void agregarVistaPartida(String dif, String veh) {
		String ruta = rutaDeMapaEnBaseADificultadString(dif);
		Vehiculo vehiculoInicial = vehiculoEnBaseAString(veh);
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		partida = null;
		partida = new Partida(mapa, new Jugador());
		xmlh.cargarArchivo(ruta, mapa);
		partida.setRanking(miRanking);
		partida.getJugador().setVehiculo(vehiculoInicial);
		mapa.setNombreJugador(getNombreJugador());
		crearVistaPartida();
	}

	private void crearVistaPartida() {
		Controlador controlador = new Controlador(partida);

		miVistaPartida = new VistaPartida(partida, controlador, this);
		
		// HORRIBLE DE MENU PRINCIPAL
		Container pane = ventanaPrincipal.getContentPane();
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

		pane.add(miVistaPartida.topPanel);
		pane.add(miVistaPartida.gamePanel);
		panelActual = null;
		
		
		ventanaPrincipal.setPreferredSize(new Dimension(1400,800)); 
		ventanaPrincipal.validate();
		ventanaPrincipal.pack(); 
		ventanaPrincipal.repaint();
		actualizarVentana();
	}
	
	public void eliminarVistaPartida() {
		ventanaPrincipal.remove(miVistaPartida.topPanel);
		ventanaPrincipal.remove(miVistaPartida.gamePanel);
		actualizarVentana();
	}

	private Vehiculo vehiculoEnBaseAString(String veh) {
		if(veh == "Auto") return new Auto();
		if(veh == "Moto") return new Moto();
		if(veh == "Camioneta4x4") return new Camioneta4x4();
		return null;
	}

	private String rutaDeMapaEnBaseADificultadString(String dif) {
		//if(dif == "Facil") 
		if (dif == "Dificil") return "src/gpschallenge/recursos/mapas/dificil.xml";
		if (dif == "Moderado") return "src/gpschallenge/recursos/mapas/moderado.xml";
		return "src/gpschallenge/recursos/mapas/facil.xml";
	}

	public void agregarVistaPartidaCargada() {
		Mapa mapa = new Mapa();
		XMLHandler xmlh = new XMLHandler();
		partida = new Partida(mapa, new Jugador());
		String ruta = "src/gpschallenge/recursos/partidas_guardadas/" + getNombreJugador() + ".xml";
		xmlh.cargarPartidaGuardada(ruta, mapa);
		partida.setRanking(miRanking);
		mapa.setNombreJugador(getNombreJugador());
		crearVistaPartida();
	}

	public String getNombreJugador() {
		return nombreJugador;
	}

	public void setNombreJugador(String nombreJugador) {
		this.nombreJugador = nombreJugador;
	}

	public Partida getPartida() {
		if(partida != null){
			return partida;
		}else {
			return null;
		}
	}
	
	private void crearRanking(String ruta) {
		miRanking = new Ranking(ruta);
		
	}

	public Ranking getRanking(){
		return miRanking;
	}
	
	public boolean tienePartidasGuardadas() {
		
		String ruta = "src/gpschallenge/recursos/partidas_guardadas/" + getNombreJugador() + ".xml";
		boolean existe = new File(ruta).exists();
		if (existe){
			return true;
		} else { return false; }
	}

	public void eliminarPartidaGuardada() {
		String ruta = "src/gpschallenge/recursos/partidas_guardadas/" + getNombreJugador() + ".xml";
		(new File(ruta)).delete();
	}

}
