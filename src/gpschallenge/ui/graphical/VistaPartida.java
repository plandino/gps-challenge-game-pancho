package gpschallenge.ui.graphical;

import gpschallenge.Partida;
import gpschallenge.mapa.Camino;
import gpschallenge.mapa.Cruce;
import gpschallenge.mapa.Cruce.Direccion;
import gpschallenge.modificador.IEfectoObtenible;
import gpschallenge.ui.Controlador;
import gpschallenge.ui.graphical.vistas.VentanaManager;
import gpschallenge.ui.graphical.vistas.VistaCanvas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.text.DecimalFormat;
import java.util.*;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class VistaPartida implements Observer {
	
	public JPanel topPanel;
	public GamePanel gamePanel;
	
    private Partida modelo;
    private Controlador control;

    private JLabel labelInfoMovimientos;
    private JLabel labelTipoVehiculo;
    private JLabel labelUltimoEfecto;

    private JLabel labelPuntaje;

    // Menu persistente, esto lo puedo sacar afuera despues.
    private JLabel labelDificultad;
    private JLabel labelNombreJugador;

    ArrayList<JButton> botonesDireccion;
    
	JPanel leftInfoPanel;
	JPanel canvasPanel;
	JPanel rightInfoPanel;
	
	VistaCanvas miCanvas;
	private VentanaManager parent;
	private JLabel labelInstruccion;
	
	
	public void update(Observable t, Object o)
	{	
		actualizarBotonesDeDireccion(rightInfoPanel);
        actualizarLabels();
        actualizarVistaCanvas();
        procesarCondicionDeVictoria();
	}
	
	private void procesarCondicionDeVictoria(){
		boolean partidaPerdida = modelo.partidaPerdida();
		boolean partidaGanada = modelo.partidaGanada();
		boolean partidaTerminada = partidaGanada || partidaPerdida;
		if(partidaTerminada) {
			parent.eliminarVistaPartida();
			if(partidaPerdida){
				parent.agregarPanelPerdedor();
			}
			if(partidaGanada) {
				parent.agregarPanelGanador();
			}
			parent.eliminarPartidaGuardada();
		}
	}
	
	private void actualizarVistaCanvas() {
		miCanvas.setVistas(modelo.getMapa(), modelo.getJugador().getVehiculo());
        miCanvas.validate();
		miCanvas.repaint();
	}

	private void actualizarBotonesDeDireccion(JPanel panelBotones) {
		//eliminar botones viejos.
		if(botonesDireccion != null ) {
			for(JButton boton: botonesDireccion) {
				panelBotones.remove(boton);
				panelBotones.invalidate();
			}
		}
		botonesDireccion = new ArrayList<JButton>();
		
		
		
		EnumMap<Direccion, Camino> caminosPosibles = modelo.getCaminosPosibles();
		if(caminosPosibles != null) {
			for(final Map.Entry<Cruce.Direccion, Camino> entry : caminosPosibles.entrySet()) {
				JButton boton = new JButton(entry.getKey().toString());
				boton.addActionListener(control.getListenerEscucharBotonMover(entry.getKey()));
				botonesDireccion.add(boton);
				boton.setPreferredSize(new Dimension(150, 30));
				
			}
			for(JButton boton : botonesDireccion) {
				panelBotones.add(boton);
			}
		}
		panelBotones.validate();
		panelBotones.repaint();
	}

    private void actualizarLabels() {
    	String nombreVehiculo = modelo.getVehiculo().getClass().toString();
    	nombreVehiculo = nombreVehiculo.substring(nombreVehiculo.lastIndexOf("."));
    	double movAct = modelo.getJugador().getContador().getCantidadDeMovimientos();
    	double limite = modelo.getJugador().getContador().getLimiteDeMovimientos();
    	double puntaje = modelo.getJugador().getPuntaje(modelo.getDificultad());
        DecimalFormat numerosConComa = new DecimalFormat("#0.00");
        String movActStr = numerosConComa.format(movAct);
    	String limiteStr = numerosConComa.format(limite);
    	
    	labelDificultad.setText("Partida "+modelo.getDificultad().getNombre());
    	ArrayList<IEfectoObtenible> efectos = modelo.getUltimosEfectos();
    	String efectosComoString = "";
    	for(IEfectoObtenible ef: efectos ) {
    		String nombreCompleto = ef.getClass().toString();
    		nombreCompleto = nombreCompleto.substring(nombreCompleto.lastIndexOf(".") + 1);
    		
    		if(ef != null) efectosComoString += nombreCompleto + "<br>";
    	}
    	labelUltimoEfecto.setText("<html><body>Ultimos Efectos:<br> "+efectosComoString+"</body></html>");
    	labelNombreJugador.setText(modelo.getMapa().getNombreJugador());  //
    	labelInfoMovimientos.setText("<html><body>Mov limites: "+limiteStr+"<br>Mov Actuales: "+movActStr+"</body></html>");
    	labelTipoVehiculo.setText("Vehiculo Actual:" + nombreVehiculo); // TODO: getNombreVehiculo o lo que fuera
    	labelPuntaje.setText("Puntaje: "+numerosConComa.format(puntaje));
    }
	
    public VistaPartida(Partida modelo, Controlador control, VentanaManager ventanaLimpia) {
    	parent = ventanaLimpia;
    	this.modelo = modelo;
    	this.control = control;
    	this.modelo.addObserver(this);
    	
    	topPanel = new JPanel();
    	topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));
    	
    	labelDificultad = new JLabel();
    	labelDificultad.setText("Partida Dificil");
    	topPanel.add(labelDificultad);
    	
    	topPanel.add(Box.createHorizontalGlue());
    	labelInstruccion = new JLabel();
    	labelInstruccion.setText("Puede moverse con las flechas del teclado o los botones de dirección");
    	topPanel.add(labelInstruccion);
    	
    	topPanel.add(Box.createHorizontalGlue());
    	labelNombreJugador = new JLabel();
    	labelNombreJugador.setText("Hola Juan");  // 
    	topPanel.add(labelNombreJugador);

    	gamePanel = new GamePanel(this.modelo);
    	gamePanel.setLayout(new BoxLayout(gamePanel, BoxLayout.X_AXIS));
    	
    	leftInfoPanel = new JPanel();
    	canvasPanel = new JPanel();
    	rightInfoPanel = new JPanel();

    	rightInfoPanel.setLayout(new BoxLayout(rightInfoPanel, BoxLayout.Y_AXIS));
    	leftInfoPanel.setLayout(new BoxLayout(leftInfoPanel, BoxLayout.Y_AXIS));
    	canvasPanel.setLayout(new BoxLayout(canvasPanel, BoxLayout.Y_AXIS));
    	
    	labelInfoMovimientos = new JLabel();
    	labelInfoMovimientos.setText("<html><body>Mov limites: 3<br>Mov Actuales: 4</body></html>");
    	leftInfoPanel.add(labelInfoMovimientos);
    	
    	labelTipoVehiculo = new JLabel();
    	labelTipoVehiculo.setText("Vehiculo Actual: Cualquiera"); // TODO: getNombreVehiculo o lo que fuera
    	leftInfoPanel.add(labelTipoVehiculo);
    	
    	labelUltimoEfecto = new JLabel();
    	labelUltimoEfecto.setText("Ultimo Efecto: ");
    	leftInfoPanel.add(labelUltimoEfecto);
    	// aca le agregarias un dibujo del auto con ImageIcon probablemente
    	miCanvas = new VistaCanvas(modelo.getMapa(), modelo.getVehiculo());
    	miCanvas.setSize(750, 650);
    	miCanvas.setBackground(Color.GREEN);
    	canvasPanel.add(miCanvas);
 
    	
    	labelPuntaje = new JLabel();
    	labelPuntaje.setText("Puntaje: 102");
    	rightInfoPanel.add(labelPuntaje);
    	rightInfoPanel.add(Box.createVerticalGlue());
    	actualizarBotonesDeDireccion(rightInfoPanel);
    	
    	gamePanel.add(leftInfoPanel, BorderLayout.EAST);
    	gamePanel.add(canvasPanel, BorderLayout.CENTER);
    	gamePanel.add(rightInfoPanel, BorderLayout.WEST);

    	Font font = new Font("Verdana", Font.BOLD, 16);
    	labelDificultad.setFont(font);
    	labelInfoMovimientos.setFont(font);
    	labelNombreJugador.setFont(font);
    	labelPuntaje.setFont(font);
    	labelTipoVehiculo.setFont(font);
    	labelUltimoEfecto.setFont(font);
    	labelInstruccion.setFont(font);

    	/*topPanel.setBackground(new Color(150,0,0));
    	leftInfoPanel.setBackground(new Color(0,0,150));
    	canvasPanel.setBackground(new Color(0,122,122));
    	rightInfoPanel.setBackground(new Color(0,150,0));
    	topPanel.setMinimumSize(new Dimension(500, 100));*/
    	modelo.actualizarObservadores();
    }
}

