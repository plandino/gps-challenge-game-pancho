package gpschallenge.ui.graphical.paneles;

import java.awt.event.ActionEvent;


import javax.swing.JPanel;

@SuppressWarnings("serial")
public abstract class CustomPanel extends JPanel {

	abstract public void procesarAccion(ActionEvent event);

}
