package gpschallenge.ui.graphical.paneles;

import gpschallenge.ui.graphical.vistas.VentanaManager;

import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class PanelPerdedor extends CustomPanel{
	
	private JButton volver;
	private JLabel titulo;
	private VentanaManager parent;

	public PanelPerdedor(VentanaManager ventanaLimpia) {
		parent = ventanaLimpia;
		titulo = new JLabel();
		titulo.setText("Seguir participando");
		
		volver = new JButton("Volver");
		volver.addActionListener(parent);
		
		this.add(titulo);
		this.add(volver);
	}

	public boolean eventoVolver(ActionEvent event) {
		return event.getSource() == volver;
	}


	public void procesarAccion(ActionEvent event) {
		//panelActual.procesarEvento(event);
		if(this.eventoVolver(event)) {
			parent.eliminarPanel(this);
			parent.agregarPanelMenuJugador();
		}
	}
}
