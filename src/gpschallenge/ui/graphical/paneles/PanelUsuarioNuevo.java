package gpschallenge.ui.graphical.paneles;

import gpschallenge.ui.graphical.vistas.VentanaManager;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class PanelUsuarioNuevo extends CustomPanel {

	private VentanaManager parent;

	private JLabel titulo;
	private JButton confirmar;
	private JTextField campoTextoNombre;
	private final String cadenaPermitidaDeCaracteres = "^[a-zA-Z0-9_]+$"; // Los caracteres permitidos son todas las letras, en minuscula o mayus y los numeros
	
	public PanelUsuarioNuevo(VentanaManager ventanaLimpia) {
		parent = ventanaLimpia;
		titulo = new JLabel("Elija nombre");
		confirmar = new JButton("Guardar");
		campoTextoNombre = new JTextField("Introduza un nombre"); // Tengo que ponerle un texto al campoTexto, porque sino al ajustarse los paneles,
		confirmar.addActionListener(parent);					  // el camp no me queda visible
		
		campoTextoNombre.addActionListener(parent);
		campoTextoNombre.setPreferredSize(new Dimension(200, 20));
		
		JLabel labelAuxiliar = new JLabel("Por favor introduzca un nombre valido, sin caracteres especiales, ni espacios vacios.");
		JPanel panel = new JPanel();
		panel.add(titulo);
		
		panel.add(labelAuxiliar);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		this.add(panel);
		this.add(campoTextoNombre);
		this.add(confirmar);
	}

	public boolean eventoGuardar(ActionEvent event) {
		return event.getSource() == confirmar;
	}

	public void procesarAccion(ActionEvent event) {
		if(this.eventoGuardar(event)) {
			
			String nombre = campoTextoNombre.getText();
			if (!(nombre.isEmpty())){ // Si el nombre introducido no es vacio
				
				if((nombre.matches(cadenaPermitidaDeCaracteres))){ // Si los caracteres introducidos no tienen algun caracter especial
					
					parent.setNombreJugador(nombre);
					parent.eliminarPanel(this);
					parent.agregarPanelMenuJugador();
				}
			} 
		}
		
	}

}
