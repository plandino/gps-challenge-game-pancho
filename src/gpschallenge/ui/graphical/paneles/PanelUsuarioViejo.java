package gpschallenge.ui.graphical.paneles;

import gpschallenge.ui.graphical.vistas.VentanaManager;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

@SuppressWarnings("serial")
public class PanelUsuarioViejo extends CustomPanel {

	JPanel panelElegirUsuario;
	private JLabel titulo;
	private JButton confirmar, usuarioNuevo;
	private VentanaManager parent;
	private ButtonGroup grupoUsuariosViejos;
	private String directorioConPartidasGuardadas = "src/gpschallenge/recursos/partidas_guardadas";
	
	public PanelUsuarioViejo(VentanaManager ventanaLimpia) {
		parent = ventanaLimpia;
		
		
		if(hayPartidasGuardadas()){
			
			titulo = new JLabel("Elija un usuario");
			confirmar = new JButton("Confirmar");
			confirmar.addActionListener(parent);
			
			crearPanelConUsuariosViejos();
			
			this.add(titulo);
			this.add(panelElegirUsuario);
			this.add(confirmar);
		} else {
			titulo = new JLabel("No hay partidas guardadas");
			usuarioNuevo = new JButton("Elegir usuario nuevo");
			usuarioNuevo.addActionListener(parent);
			this.add(titulo);
			this.add(usuarioNuevo);
		}	

	}
	
	public boolean eventoGuardar(ActionEvent event) {
		return event.getSource() == confirmar;
	}
	
	public boolean eventoUsuarioNuevo(ActionEvent event){
		return event.getSource() == usuarioNuevo;
	}

	public void procesarAccion(ActionEvent event) {
		if(this.eventoGuardar(event)) {
			parent.setNombreJugador(GroupButtonUtils.getSelectedButtonText(grupoUsuariosViejos));
			parent.eliminarPanel(this);
			parent.agregarPanelMenuJugador();
		}
		if(this.eventoUsuarioNuevo(event)){
			parent.eliminarPanel(this);
			parent.agregarPanelUsuarioNuevo();
		}
		
	}
	
	public boolean hayPartidasGuardadas(){
		File unFile = new File(directorioConPartidasGuardadas);
		File[] ficheros = unFile.listFiles();
		return (ficheros.length>0); // Si hay archivos en partidas_guardadas devuelve true
	}
	
	public void crearPanelConUsuariosViejos(){
		grupoUsuariosViejos = new ButtonGroup();
		panelElegirUsuario = new JPanel();
		
		File unFile = new File(directorioConPartidasGuardadas);
		if (unFile.exists()){ // Directorio existe 
			File[] ficheros = unFile.listFiles();
			for (int x=0;x<ficheros.length;x++){
				String [] arreglo = cortarStringsPorPuntos(ficheros[x].getName());
				JRadioButton botonAuxiliar = new JRadioButton(arreglo[0]);
				grupoUsuariosViejos.add(botonAuxiliar);
				panelElegirUsuario.add(botonAuxiliar);
				botonAuxiliar.setSelected(true);
			}
		}	else { System.out.println("Hubo un error, el directorio de partidas guardadas no existe"); }
		panelElegirUsuario.setLayout(new BoxLayout(panelElegirUsuario, BoxLayout.Y_AXIS));
	}
	
	public String[] cortarStringsPorPuntos(String cadena){
		  return cadena.split("\\.");
}
	

}
