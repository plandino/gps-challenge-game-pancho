package gpschallenge.ui.graphical.paneles;

import gpschallenge.ranking.Puntaje;
import gpschallenge.ui.graphical.vistas.VentanaManager;

import java.awt.event.ActionEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class PanelPuntajes extends CustomPanel {

	private VentanaManager parent;
	private JLabel titulo;
	private JButton volver;
	
	public PanelPuntajes(VentanaManager ventanaLimpia) {
		parent = ventanaLimpia;

		verPuntajes();
		
		volver = new JButton("Volver");		
		volver.addActionListener(parent);		
		this.add(volver);
	}
	
	public boolean eventoVolver(ActionEvent event) {
		return event.getSource() == volver;
	}

	public void procesarAccion(ActionEvent event) {
		if(this.eventoVolver(event)) {
			parent.eliminarPanel(this);
			parent.agregarPanelMenuJugador();
		}
	}
	
	public void verPuntajes() {
		
		ArrayList<Puntaje> listaDePuntajes = parent.getRanking().getListaPuntajes();
		if (listaDePuntajes.isEmpty()){
			titulo = new JLabel();
			titulo.setText("El ranking esta vacio");
			this.add(titulo);
		} else {
			titulo = new JLabel();
			titulo.setText("Nombre Puntaje");
			this.add(titulo);
			
			for(Puntaje unPuntaje : listaDePuntajes){
				JLabel puntaje = new JLabel();
				DecimalFormat numerosConComa = new DecimalFormat("#0.00");
				puntaje.setText(unPuntaje.getNombre() + " " + numerosConComa.format(unPuntaje.getPuntaje()));
				this.add(puntaje);	
			}
		}
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
	}	

}
