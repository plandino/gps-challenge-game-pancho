package gpschallenge.ui.graphical.paneles;

import gpschallenge.ui.graphical.vistas.VentanaManager;

import java.awt.event.ActionEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;


@SuppressWarnings("serial")
public class PanelComenzarPartida extends CustomPanel {

	private VentanaManager parent;
	JPanel panelDificultad;
	JPanel panelVehiculos;
	JRadioButton facil, moderado, dificil;
	JRadioButton auto, camioneta4x4, moto;
	private ButtonGroup grupoDificultad;
	private ButtonGroup grupoVehiculos;
	
	JButton comenzar;
	
	public PanelComenzarPartida(VentanaManager ventanaLimpia) {
		parent = ventanaLimpia;
		crearPanelDificultad();
		crearPanelVehiculos();
		comenzar = new JButton("Comenzar partida");
		
		comenzar.addActionListener(parent);
		
		this.add(panelDificultad);
		this.add(panelVehiculos);
		this.add(comenzar);
		
		dificil.setSelected(true);
		auto.setSelected(true);
	}
	
	private void crearPanelDificultad() {
		grupoDificultad = new ButtonGroup();
		
		panelDificultad = new JPanel();
		
		facil=new JRadioButton("Facil");
		facil.setBounds(10,10,150,30);
		facil.addChangeListener(parent);
		moderado=new JRadioButton("Moderado");
		moderado.setBounds(10,10,150,30);
		moderado.addChangeListener(parent);
		dificil=new JRadioButton("Dificil");
		dificil.setBounds(10,10,150,30);
		dificil.addChangeListener(parent);
		
		grupoDificultad.add(facil);
		grupoDificultad.add(moderado);
		grupoDificultad.add(dificil);
		
		panelDificultad.add(facil);
		panelDificultad.add(moderado);
		panelDificultad.add(dificil);
		
		panelDificultad.setLayout(new BoxLayout(panelDificultad, BoxLayout.Y_AXIS));
	}

	private void crearPanelVehiculos() {
		grupoVehiculos = new ButtonGroup();
		
		panelVehiculos = new JPanel();
		
		auto=new JRadioButton("Auto");
		auto.setBounds(10,10,150,30);
		auto.addChangeListener(parent);
		camioneta4x4=new JRadioButton("Camioneta4x4");
		camioneta4x4.setBounds(10,10,150,30);
		camioneta4x4.addChangeListener(parent);
		moto=new JRadioButton("Moto");
		moto.setBounds(10,10,150,30);
		moto.addChangeListener(parent);
		
		grupoVehiculos.add(auto);
		grupoVehiculos.add(camioneta4x4);
		grupoVehiculos.add(moto);
		
		panelVehiculos = new JPanel();
		panelVehiculos.add(auto);
		panelVehiculos.add(camioneta4x4);
		panelVehiculos.add(moto);
		
		panelVehiculos.setLayout(new BoxLayout(panelVehiculos, BoxLayout.Y_AXIS));
	}
	@Override
	public void procesarAccion(ActionEvent event) {
		if(event.getSource() == comenzar) {
			parent.eliminarPanel(this);
			String dificultadString = GroupButtonUtils.getSelectedButtonText(grupoDificultad);
			String vehiculoString = GroupButtonUtils.getSelectedButtonText(grupoVehiculos);
			parent.agregarVistaPartida(dificultadString, vehiculoString);
		}
		
	}

}
