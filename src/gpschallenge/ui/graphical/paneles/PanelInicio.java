package gpschallenge.ui.graphical.paneles;

import gpschallenge.ui.graphical.vistas.VentanaManager;

import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class PanelInicio extends CustomPanel {
	private JLabel titulo;
	private JButton usuarioNuevo;
	private JButton usuarioViejo;
	private VentanaManager parent;
	
	public PanelInicio(VentanaManager ventanaLimpia) {
		parent = ventanaLimpia;
		// TODO Auto-generated constructor stub
		titulo = new JLabel();
		titulo.setText("Welcome!");
		
		usuarioNuevo = new JButton("Soy Nuevo");
		usuarioViejo = new JButton("Ya tengo usuario");
		
		usuarioNuevo.addActionListener(parent);
		usuarioViejo.addActionListener(parent);
		
		this.add(titulo);
		this.add(usuarioNuevo);
		this.add(usuarioViejo);
	}

	public boolean eventoUsuarioNuevo(ActionEvent event) {
		// TODO Auto-generated method stub
		return event.getSource() == usuarioNuevo;
	}

	public boolean eventoUsuarioViejo(ActionEvent event) {
		// TODO Auto-generated method stub
		return event.getSource() == usuarioViejo;
	}

	public void procesarAccion(ActionEvent event) {
		//panelActual.procesarEvento(event);
		if(this.eventoUsuarioNuevo(event)) {
			parent.eliminarPanel(this);
			parent.agregarPanelUsuarioNuevo();
		}
		if(this.eventoUsuarioViejo(event)) {
			parent.eliminarPanel(this);
			parent.agregarPanelUsuarioViejo();
		}
	}
	
	
}
