package gpschallenge.ui.graphical.paneles;

import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;

public class GroupButtonUtils {

	/**
	 * Devuelve el texto del boton que esta seleccionado dentro del grupo de botones que me pasan por parametro
	 * @param buttonGroup: Grupo de botones del cual tomar el que esta seleccionado
	 * @return: El texto del boton que esta seleccionado
	 */
    static public String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }

        return null;
    }
}