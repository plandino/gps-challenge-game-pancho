package gpschallenge.ui.graphical.paneles;

import gpschallenge.Partida;
import gpschallenge.ui.graphical.vistas.VentanaManager;

import java.awt.event.ActionEvent;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class PanelGanador extends CustomPanel{

	private JButton volver;
	private JLabel titulo;
	private VentanaManager parent;

	public PanelGanador(VentanaManager ventanaLimpia) {
		parent = ventanaLimpia;
		// TODO Auto-generated constructor stub
		titulo = new JLabel();
		titulo.setText("Felicitaciones, ganaste!");
		
		volver = new JButton("Volver");
		volver.addActionListener(parent);
		
		this.add(titulo);
		mostrarPuntajeFinal();
		this.add(volver);
	}

	public boolean eventoVolver(ActionEvent event) {
		// TODO Auto-generated method stub
		return event.getSource() == volver;
	}
	
	public void mostrarPuntajeFinal(){
		JLabel labelAuxiliar = new JLabel();
		Partida partida = parent.getPartida();
		double puntaje = partida.getJugador().getPuntaje(partida.getDificultad());
		double movimientosRestantes = partida.getJugador().getContador().getCantidadDeMovimientosSobrantes();
		DecimalFormat numerosConComa = new DecimalFormat("#0.00");
		labelAuxiliar.setText("Has hecho un puntaje de  " + numerosConComa.format(puntaje) + ". Te sobraron " + numerosConComa.format(movimientosRestantes) + " movimientos");
		this.add(labelAuxiliar);
	}

	public void procesarAccion(ActionEvent event) {
		//panelActual.procesarEvento(event);
		if(this.eventoVolver(event)) {
			parent.eliminarPanel(this);
			parent.agregarPanelMenuJugador();
		}
	}
}
