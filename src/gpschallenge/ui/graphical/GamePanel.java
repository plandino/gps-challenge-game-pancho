package gpschallenge.ui.graphical;

import gpschallenge.Partida;
import gpschallenge.mapa.Camino;
import gpschallenge.mapa.Cruce.Direccion;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

@SuppressWarnings("serial")
public class GamePanel extends JPanel {

	   private Partida modelo;

	public GamePanel(Partida modelo) {
	      setKeyBindings();
	      this.modelo = modelo;
	   }

	   private void setKeyBindings() {
	      ActionMap actionMap = getActionMap();
	      int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
	      InputMap inputMap = getInputMap(condition );

	      String vkLeft = "VK_LEFT";
	      String vkRight = "VK_RIGHT";
	      String vkUp = "VK_UP";
	      String vkDown = "VK_DOWN";
	      inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), vkLeft);
	      inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), vkRight);
	      inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), vkUp);
	      inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), vkDown);

	      actionMap.put(vkLeft, new KeyAction(vkLeft));
	      actionMap.put(vkRight, new KeyAction(vkRight));
	      actionMap.put(vkDown, new KeyAction(vkDown));
	      actionMap.put(vkUp, new KeyAction(vkUp));

	   }
	   
	   Direccion getDireccionEnBaseAString(String str) {
		   if(str=="VK_LEFT") return Direccion.IZQUIERDA;
		   if(str=="VK_RIGHT") return Direccion.DERECHA;
		   if(str=="VK_UP") return Direccion.ARRIBA;
		   if(str=="VK_DOWN") return Direccion.ABAJO;
		   return null;
	   }

	   @Override
	   public Dimension getPreferredSize() {
	      return new Dimension(300, 200);
	   }

	   private class KeyAction extends AbstractAction {
	      public KeyAction(String actionCommand) {
	         putValue(ACTION_COMMAND_KEY, actionCommand);
	      }

	      @Override
	      public void actionPerformed(ActionEvent actionEvt) {
	    	  if(modelo != null) {
	    		  Direccion dir = getDireccionEnBaseAString(actionEvt.getActionCommand());
	    		  if(dir != null) {
	    			  if(modelo.getPosicionVehiculo().tieneCaminoEnDireccion(dir)) {
	    				  Camino camino = modelo.getPosicionVehiculo().getCamino(dir);
	    			      modelo.moverJugadorPor(camino);
	    			  }
	    		  }
	    	 }
	      }
	   }

	}