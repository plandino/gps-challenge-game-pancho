package gpschallenge.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EnumMap;

import gpschallenge.Partida;
import gpschallenge.mapa.Camino;
import gpschallenge.mapa.Cruce;
import gpschallenge.mapa.Cruce.Direccion;

/**
 * 
 * @author Alex Hans
 *
 * Clase controladora para el MVC (junto con VistaPartida y Partida (modelo)
 * 
 */
public class Controlador {
	
	private Partida modelo;
	
	public Controlador(Partida miPartida) {
		this.modelo = miPartida;
	}
	
	/**
	 * Escucha los botones de direccion y mueve por el camino elegido 
	 */
	private class EscuchaBotonMover implements ActionListener 
	{
		private final Cruce.Direccion direccion;

		public EscuchaBotonMover(final Cruce.Direccion dir) {
			super();
			this.direccion = dir;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			EnumMap<Direccion, Camino> caminosSalientes = modelo.getMapa().getPosicionVehiculo().getCaminosSalientes();
			Camino camino = caminosSalientes.get(this.direccion);	
			modelo.moverJugadorPor(camino);
		}
	
	}
	
	public ActionListener getListenerEscucharBotonMover(final Cruce.Direccion dir) {
		return new EscuchaBotonMover(dir);
	}

}
