package gpschallenge;

import gpschallenge.dificultad.Dificultad;
import gpschallenge.vehiculo.Auto;
import gpschallenge.vehiculo.Vehiculo;

/**
 * Los datos correspondientes a cada usuario que juega.
 * @author Francisco Landino
 *
 */
public class Jugador {
	/**
	 * El vehiculo que le corresponde en el juego
	 */
	private Vehiculo miVehiculo;
	/**
	 * Un contador para los movimientos del jugador, con su limite y sus movimientos actuales.
	 */
	private ContadorDeMovimientos contadorDeMovimientos;
	/**
	 * El nombre correspondiente al jugador
	 */
	private String nombre;

	/**
	 * Constructor que asigna un Auto como vehiculo por defecto
	 */
	public Jugador() {
		setVehiculo(new Auto());
		contadorDeMovimientos = new ContadorDeMovimientos(0.0);
		this.nombre = "JohnDoe";
	}
	/**
	 * Se construye a partir de un nombre dado, y asigna un Auto como vehiculo por defecto.
	 * @param nombre
	 */
	public Jugador(String nombre) {
		setVehiculo(new Auto());
		this.contadorDeMovimientos = new ContadorDeMovimientos(0.0);
		this.nombre = nombre;
	}

	/**
	 * Devuelve el vehiculo que el Jugador posee.
	 * @return
	 */
	public Vehiculo getVehiculo() {
		return miVehiculo;
	}

	/**
	 * Fija un vehiculo para el jugador.
	 * @param miVehiculo
	 */
	public void setVehiculo(Vehiculo miVehiculo) {
		this.miVehiculo = miVehiculo;
	}

	/**
	 * Devuelve el contador de movimientos del jugador.
	 * @return
	 */
	public ContadorDeMovimientos getContador() {
		
		// TODO Auto-generated method stub
		return contadorDeMovimientos;
	}

	/**
	 * Suma una determinada cantidad de movimientos a los ya existentes en el jugador
	 * @param adicionDeMovimientos la cantidad de movimientos a sumar
	 */
	public void sumaDeMovimientos(double adicionDeMovimientos) {
		// TODO Auto-generated method stub
		contadorDeMovimientos.sumarCantidadDeMovimientos(adicionDeMovimientos);
	}

	/**
	 * Multiplica los movimientos del jugador por un porcentaje de los mismos.
	 * @param productoDeMovimientos El porcentaje a multiplicar.
	 */
	public void productoDeMovimientos(double productoDeMovimientos) {
		// TODO Auto-generated method stub
		contadorDeMovimientos.productoCoef(productoDeMovimientos);
	}

	/**
	 * Devuelve el puntaje actual del jugador, en base a una dificultad determinada.
	 * @param dificultad
	 * @return
	 */
	public double getPuntaje(Dificultad dificultad) {
		// TODO Auto-generated method stub
		double realizados = getContador().getCantidadDeMovimientos();
		double limite = getContador().getLimiteDeMovimientos();
		return dificultad.puntajeEnBaseAMovimientos(realizados, limite);
	}

	/**
	 * Devuelve el nombre del jugador
	 * @return
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Fija un nombre al jugador.
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
