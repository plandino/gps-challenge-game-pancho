package gpschallenge.modificador.obstaculo;

import gpschallenge.Partida;
import gpschallenge.modificador.*;

abstract public class Obstaculo implements IEfectoObtenible {
    abstract public Modificador obtenerModificador(Partida partida);
}
