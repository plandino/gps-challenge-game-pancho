package gpschallenge.modificador.obstaculo;

import gpschallenge.Partida;
import gpschallenge.modificador.*;

/**
 * Un obstaculo que agrega 3 movimientos al puntaje cuando se activa.
 *
 */
public class Pozo extends Obstaculo {
	public Modificador obtenerModificador(Partida partida) {
		return partida.getJugador().getVehiculo().obtenerModificador(this);
	} 
}
