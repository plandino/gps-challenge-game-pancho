package gpschallenge.modificador.obstaculo;


import gpschallenge.Partida;
import gpschallenge.modificador.*;

/**
 * Un obstaculo que impide que los autos y las camionetas 4x4 avancen. Las motos pueden pasar pero son penalizadas con 2 movimientos.
 *
 */
public class Piquete extends Obstaculo {
	public Modificador obtenerModificador(Partida partida) {
		return partida.getJugador().getVehiculo().obtenerModificador(this);
	}
}
