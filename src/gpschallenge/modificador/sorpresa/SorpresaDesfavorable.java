package gpschallenge.modificador.sorpresa;

import gpschallenge.Partida;
import gpschallenge.modificador.Modificador;

/**
 * Una sorpresa que al activarse modifica el puntaje agregando un 25% de movimientos.
 *
 */
public class SorpresaDesfavorable extends Sorpresa {

	@Override
	public Modificador obtenerModificador(Partida partida) {
		Modificador mod = new Modificador();
		mod.productoDeMovimientos = 0.25;
		return mod;
	}
	
}
