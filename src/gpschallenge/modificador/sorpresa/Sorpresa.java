package gpschallenge.modificador.sorpresa;

import gpschallenge.modificador.*;
import gpschallenge.Partida;

abstract public class Sorpresa implements IEfectoObtenible {
    abstract public Modificador obtenerModificador(Partida partida);
}
