package gpschallenge.modificador.sorpresa;

import gpschallenge.Partida;
import gpschallenge.modificador.Modificador;
/**
 * Una sorpresa que al activarse cambia el vehiculo actual. La regla para los cambios es la siguiente:
 * <pre> Moto -> Auto
 * Auto -> Camioneta 4x4
 * Camioneta 4x4 -> Moto </pre>
 *
 */
public class SorpresaCambioDeVehiculo extends Sorpresa {

	@Override
	public Modificador obtenerModificador(Partida partida) {
		Modificador mod = new Modificador();
		mod.cambioDeVehiculo = true;
		return mod;
	}
	
}
