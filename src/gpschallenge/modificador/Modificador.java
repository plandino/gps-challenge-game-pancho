package gpschallenge.modificador;

/*
 * 
 * @author: Alex
 * Estructura de reglas para aplicar el resultado de un 
 * efecto.  
 * 
 * Puede ser una adicion de movimientos, un producto,
 * cambios de vehiculos o un movimientoExitoso.
 *  */
public class Modificador {
    public double adicionDeMovimientos;
    public double productoDeMovimientos;
    public boolean cambioDeVehiculo;
    public boolean movimientoExitoso;
    
    public Modificador() {
    	adicionDeMovimientos = 0;
    	productoDeMovimientos = 0.0;
    	cambioDeVehiculo = false;
    	movimientoExitoso = true;
    }
    
    @Override
    public boolean equals(Object ob) {
    	if(ob == null) return false;
    	if(ob.getClass() != getClass()) return false;
    	Modificador modif = (Modificador)ob;
    	if(Double.compare(adicionDeMovimientos, modif.adicionDeMovimientos) == 0 &&
    	   Double.compare(productoDeMovimientos, modif.productoDeMovimientos) == 0 &&
    	   cambioDeVehiculo == modif.cambioDeVehiculo &&
    	   movimientoExitoso == modif.movimientoExitoso
    	) return true;
    	
    	return false;
    }
    @Override
    public int hashCode() {
    	return 	(new Double(adicionDeMovimientos)).hashCode() ^
    			(new Double(productoDeMovimientos)).hashCode() ^
    			(new Boolean(cambioDeVehiculo)).hashCode() ^
    			(new Boolean(movimientoExitoso)).hashCode();
    }
} 
