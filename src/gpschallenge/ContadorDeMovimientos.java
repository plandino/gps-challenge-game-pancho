package gpschallenge;
/**
 * Una clase que se encarga de llevar la cuenta de los movimientos de un jugador.
 * @author Francisco Landino
 *
 */
public class ContadorDeMovimientos {
	private double cantidadDeMovimientos;
	private double limiteDeMovimientos; 
	
	/**
	 * Permite inicializar un contador con una cantidad de movimientos inicial.
	 * @param d La cantidad inicial que se fijara.
	 */
	public ContadorDeMovimientos(double d){
		//Se inicializa con cero movimientos
		setCantidad(d);
		limiteDeMovimientos = 100.0;
	}
	
	/**
	 * Reinicia el contador.
	 */
	public void resetACero(){
		setCantidad(0.0);
	}
	
	/**
	 * Suma una determinada cantidad de movimientos a los ya existentes.
	 * @param cantidadDeMovimientosASumar
	 */
	public void sumarCantidadDeMovimientos(double cantidadDeMovimientosASumar){
		setCantidad(this.cantidadDeMovimientos + cantidadDeMovimientosASumar);
	}
	
	/**
	 * Agrega un porcentaje {@code d} a los puntos existentes
	 * @param d
	 */
	public void productoCoef(double d) {
		setCantidad(cantidadDeMovimientos * (1.0 + d));
	}
	
	public double getCantidadDeMovimientos(){
		return this.cantidadDeMovimientos;
	}
	/**
	 * Fija la cantidad de movimientos del contador
	 * @param d
	 */
	public void setCantidad(double d) {
		// TODO Auto-generated method stub
		cantidadDeMovimientos = d;
	}
	/**
	 * Devuelve el limite de movimientos posibles.
	 * @return
	 */
	public double getLimiteDeMovimientos() {
		return limiteDeMovimientos;
	}
	/**
	 * Fija un numero de movimientos maximos.
	 * @param limiteDeMovimientos
	 */
	public void setLimiteDeMovimientos(double limiteDeMovimientos) {
		this.limiteDeMovimientos = limiteDeMovimientos;
	}
	/**
	 * Se fija si el jugador supero o igualo los movimientos limites.
	 * @return
	 */
	public boolean noTieneMasMovimientosDisponibles() {
		// TODO Auto-generated method stub
		return this.limiteDeMovimientos < this.cantidadDeMovimientos;
	}
	
	/**
	 * Devuelve los movimientos sobrantes con respecto al limite.
	 * @return
	 */
	public double getCantidadDeMovimientosSobrantes(){
		return (limiteDeMovimientos - cantidadDeMovimientos);
	}
}
