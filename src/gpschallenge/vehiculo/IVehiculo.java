package gpschallenge.vehiculo;

import gpschallenge.modificador.Modificador;
import gpschallenge.modificador.obstaculo.*;


public interface IVehiculo {
    public IVehiculo vehiculoDeCambio();

    // Obstaculos
	public Modificador obtenerModificador(Piquete piquete);
	public Modificador obtenerModificador(Pozo pozo);
	public Modificador obtenerModificador(ControlPolicial control);
     
}