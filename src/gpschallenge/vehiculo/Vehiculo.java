package gpschallenge.vehiculo;

import gpschallenge.modificador.Modificador;
import gpschallenge.modificador.obstaculo.ControlPolicial;

/**
 * Implementa la interfaz vehiculo y se resonsabliza del
 * comportamiento común de los vehiculos
 */
public abstract class Vehiculo implements IVehiculo {
	
	/**
	 * Si pasa por un control policial, esta sera la probabilidad
	 * de sufrir consecuencias.
	 */
	private double probabilidadDeSerDemoradoPorControl;
	
	
	public Vehiculo() {
		setProbabilidadDeSerDemoradoPorControl(1.0);
	}
		
	public double getProbabilidadDeSerDemoradoPorControl() {
		return probabilidadDeSerDemoradoPorControl;
	}
	
	public void setProbabilidadDeSerDemoradoPorControl(double prob) {
		probabilidadDeSerDemoradoPorControl = prob;
	}

	@Override
	/**
	 * Comportamiento general con respecto a controles policiales.
	 * agrega 3 a la Modificador.adicionDeMovimientos si se da la probablidad
	 * del auto especifico (con Math.Random). 
	 */
	public Modificador obtenerModificador(ControlPolicial control) {
		// TODO Auto-generated method stub
		Modificador mod = new Modificador();
		double numeroAleatorio = Math.random();
		if(numeroAleatorio < this.getProbabilidadDeSerDemoradoPorControl() ) {
	        mod.adicionDeMovimientos = 3;
		}
		return mod;
	}
}
