package gpschallenge.vehiculo;

import gpschallenge.modificador.Modificador;
import gpschallenge.modificador.obstaculo.Piquete;
import gpschallenge.modificador.obstaculo.Pozo;
import gpschallenge.vehiculo.Camioneta4x4;

public class Auto extends Vehiculo {

	public Auto() {
		super();
		this.setProbabilidadDeSerDemoradoPorControl(0.5);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public IVehiculo vehiculoDeCambio() {
		// TODO Auto-generated method stub
		return new Camioneta4x4();
	}

	@Override
	public Modificador obtenerModificador(Piquete piquete) {
		// TODO Auto-generated method stub
		Modificador mod = new Modificador();
        mod.movimientoExitoso = false;
		return mod;
	}

	@Override
	public Modificador obtenerModificador(Pozo pozo) {
		// TODO Auto-generated method stub
		Modificador mod = new Modificador();
        mod.adicionDeMovimientos = 3;
		return mod;
	}

}
