package gpschallenge.vehiculo;


import gpschallenge.modificador.Modificador;
import gpschallenge.modificador.obstaculo.Piquete;
import gpschallenge.modificador.obstaculo.Pozo;
import gpschallenge.vehiculo.Moto;

public class Camioneta4x4 extends Vehiculo {

	public Camioneta4x4() {
		super();
		this.setProbabilidadDeSerDemoradoPorControl(0.3);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public IVehiculo vehiculoDeCambio() {
		// TODO Auto-generated method stub
		return new Moto();
	}

	@Override
	public Modificador obtenerModificador(Piquete piquete) {
		// TODO Auto-generated method stub
		Modificador mod = new Modificador();
        mod.movimientoExitoso = false;
		return mod;
	}

	@Override

	public Modificador obtenerModificador(Pozo pozo) {
		// TODO Auto-generated method stub
		return new Modificador();
	}
	
}
