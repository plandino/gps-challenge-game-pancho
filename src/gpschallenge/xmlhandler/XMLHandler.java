package gpschallenge.xmlhandler;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import gpschallenge.dificultad.Dificultad;
import gpschallenge.dificultad.DificultadDificil;
import gpschallenge.dificultad.DificultadFacil;
import gpschallenge.dificultad.DificultadModerada;
import gpschallenge.mapa.Camino;
import gpschallenge.mapa.Cruce;
import gpschallenge.mapa.Mapa;
import gpschallenge.modificador.IEfectoObtenible;
import gpschallenge.modificador.sorpresa.*;
import gpschallenge.modificador.obstaculo.*;

import java.util.Map.Entry;

/**
 * Clase usada para el manejo de la carga y guardado de XML en el proyecto. El mismo se encarga de crear los archivos 
 * XML correspondientes y de cargarlos, para luego darle a Mapa el nodo correspondiente para que este se cargue.
 * @author Santiago
 *
 */
public class XMLHandler {
	
	/**
	 * Carga el archivo que contiene la informacion del Mapa y se la da a este para que proceda a 
	 * tomar sus propiedades correspondientes.
	 * @param rutaArchivo El String correspondiente a la ruta del archivo a cargar
	 * @param mapa el Mapa sobre el cual se van a cargar las propiedades.
	 */
	public Element cargarArchivo(String rutaArchivo, Mapa mapa) {
		File archivoXML = new File(rutaArchivo);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(archivoXML);
			Element nodoMapa = (Element)doc.getElementsByTagName("mapa").item(0);
			mapa.cargarMapa(nodoMapa);
			return nodoMapa;
		
		} catch (Exception e) {
			System.out.println("Hubo un error en la carga del archivo");
			e.printStackTrace();
			return null;
			}
		
	}
	public void cargarPartidaGuardada(String rutaArchivo, Mapa mapa) {
		Element nodoMapa = this.cargarArchivo(rutaArchivo, mapa);
		mapa.cargarDatosPartidaGuardada(nodoMapa);
	}
	
	/**
	 * Crea un archivo XML donde guarda las propiedades y el estado de un mapa indicado como parametro.
	 * El nombre del archivo corresponde al nombre del jugador al cual le corresponde el mapa.
	 * @param mapa El mapa que se va a guardar
	 * @param pathArchivo La ruta hacia la carpeta donde se va a guardar el archivo, con un "/" al final. Ejemplo
	 * <pre>
	 * 		guardarArchivo(mapa, "mi/ruta/"); </pre>
	 */
	public void guardarArchivo(Mapa mapa, String pathArchivo) {
		
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			Element nodoRaiz = doc.createElement("mapa");
			
			//Jugador
			nodoRaiz.appendChild(crearNodoJugador(doc, mapa));
			
			//Dimensiones
			nodoRaiz.appendChild(crearNodoDimensiones(doc, mapa));
			
			//Limite movimientos
			nodoRaiz.appendChild(crearNodoLimiteMov(doc, mapa));
			
			//Movimientos actuales

			nodoRaiz.appendChild(crearNodoMovimientos(doc, mapa));
			
			//Dificultad
			nodoRaiz.appendChild(crearNodoDificultad(doc, mapa));
			
			//Cruces
			for (Cruce cruce : mapa.getCruces() ) {
				nodoRaiz.appendChild(crearNodoCruces(doc, cruce));
			}
			
			//Caminos
			for (Camino camino : mapa.getCaminos() ) {
				nodoRaiz.appendChild(crearNodoCamino(doc, camino));
			}
			
			//PosicionActual
			nodoRaiz.appendChild(crearNodoPosicion(doc, mapa));
			
			//Meta
			nodoRaiz.appendChild(crearNodoMeta(doc, mapa));
			
			doc.appendChild(nodoRaiz);
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			String nombreJugador = mapa.getNombreJugador();
			String nombreArchivo = pathArchivo + nombreJugador + ".xml";
			StreamResult result = new StreamResult(new File(nombreArchivo));
			
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			transformer.transform(source, result);
			
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
			System.out.println("HUBO UN ERROR EN PCE");
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
			System.out.println("HUBO UN ERROR EN TFE");
		  }
	}
	
	
	private Element crearNodoMeta(Document doc, Mapa mapa) {
		Element nMeta = doc.createElement("meta");
		String idMeta = Integer.toString(mapa.getPosicionMeta().getIdentificador());
		nMeta.appendChild(doc.createTextNode(idMeta));
		return nMeta;
	}
	private Element crearNodoPosicion(Document doc, Mapa mapa) {
		Element nPosicionActual = doc.createElement("posicion");
		String idPosicionActual = Integer.toString(mapa.getPosicionVehiculo().getIdentificador());
		nPosicionActual.appendChild(doc.createTextNode(idPosicionActual));
		return nPosicionActual;
	}
	private Element crearNodoCamino(Document doc, Camino camino) {
		Element nCamino = doc.createElement("camino");
		Element nCruceA = doc.createElement("cruceA");
		Element nIdA = doc.createElement("id");
		nIdA.appendChild(doc.createTextNode( Integer.toString(camino.getCruces().getLeft().getIdentificador()) ));
		nCruceA.appendChild(nIdA);
		nCamino.appendChild(nCruceA);
		
		Element nCruceB = doc.createElement("cruceB");
		Element nIdB = doc.createElement("id");
		nIdB.appendChild(doc.createTextNode( Integer.toString(camino.getCruces().getRight().getIdentificador()) ));
		nCruceB.appendChild(nIdB);
		nCamino.appendChild(nCruceB);
		
		Element nNombreDesdeA = doc.createElement("nombreDesdeA");
		boolean encontrado = false;
		String nombreDesdeA = "";
		for (Entry<Cruce.Direccion, Camino> entry : camino.getCruces().getLeft().getCaminosSalientes().entrySet()) {
			if (camino.equals(entry.getValue()) && !encontrado) {
				nombreDesdeA = entry.getKey().toString();
				encontrado = true;
			}
		}
		nNombreDesdeA.appendChild(doc.createTextNode(nombreDesdeA));
	
		
		Element nNombreDesdeB = doc.createElement("nombreDesdeB");
		encontrado = false;
		String nombreDesdeB = "";
		for (Entry<Cruce.Direccion, Camino> entry : camino.getCruces().getRight().getCaminosSalientes().entrySet()) {
			if (camino.equals(entry.getValue()) && !encontrado) {
				nombreDesdeB = entry.getKey().toString();
				encontrado = true;
			}
		}
		
		nNombreDesdeB.appendChild(doc.createTextNode(nombreDesdeB));
		
		nCamino.appendChild(nNombreDesdeA);
		nCamino.appendChild(nNombreDesdeB);
		
		//EfectosObtenibles
		for (IEfectoObtenible efecto : camino.getEfectosEnOrden() ) {
			String nombreEfecto = stringAPartirDeEfectos(efecto);
			if (!nombreEfecto.isEmpty()) {
				Element nEfecto = doc.createElement("efecto");
				nEfecto.appendChild(doc.createTextNode(nombreEfecto));
				nCamino.appendChild(nEfecto);
			}
		}
		return nCamino;
		
	}
	private Element crearNodoCruces(Document doc, Cruce cruce) {
		Element nCruce = doc.createElement("cruce");
		Element nId = doc.createElement("identificador");
		nId.appendChild(doc.createTextNode(Integer.toString(cruce.getIdentificador() ) ) );
		nCruce.appendChild(nId);
		return nCruce;
	}
	private Element crearNodoDificultad(Document doc, Mapa mapa) {
		Element nDificultad = doc.createElement("dificultad");
		String dif = stringAPartirDeDificultad(mapa.getDificultad());
		nDificultad.appendChild(doc.createTextNode(dif));
		return nDificultad;
	}
	private Element crearNodoMovimientos(Document doc, Mapa mapa) {
		Element nMovimientos = doc.createElement("movimientos");
		String movimientos = Double.toString(mapa.getJugador().getContador().getCantidadDeMovimientos());
		nMovimientos.appendChild(doc.createTextNode(movimientos));
		return nMovimientos;
	}
	private Element crearNodoLimiteMov(Document doc, Mapa mapa) {
		Element nLimiteMov = doc.createElement("limiteMovimientos");
		String limitemov = Double.toString(mapa.getLimiteMovimientos());
		nLimiteMov.appendChild(doc.createTextNode(limitemov));
		return nLimiteMov;
	}
	private Element crearNodoDimensiones(Document doc, Mapa mapa) {
		Element nDimensiones = doc.createElement("dimensiones");
		Element nAncho = doc.createElement("ancho");
		String ancho = Integer.toString(mapa.getAncho() - 1);
		nAncho.appendChild(doc.createTextNode(ancho) );
		
		Element nAlto = doc.createElement("alto");
		String alto = Integer.toString(mapa.getAlto() - 1);
		nAlto.appendChild(doc.createTextNode(alto));
		
		nDimensiones.appendChild(nAncho);
		nDimensiones.appendChild(nAlto);
		
		return nDimensiones;
	}
	private Element crearNodoJugador(Document doc, Mapa mapa) {
		Element nJugador = doc.createElement("jugador");
		nJugador.appendChild(doc.createTextNode(mapa.getNombreJugador()));
		return nJugador;
	}

	
	/**
	 * Devuelve un string correspondiente al nombre de la clase de un efecto ingresado como parametro, para
	 * su correcto guardado en un XML.
	 * @param efecto El efecto cuyo nombre de clase se quiere obtener.
	 * @return El string correspondiente al nombre de la clase del efecto ingresado.
	 */
	private String stringAPartirDeEfectos(IEfectoObtenible efecto) {
		if (efecto == null) return "";
		if (efecto.getClass().equals(SorpresaFavorable.class)) return "SorpresaFavorable";
		if (efecto.getClass().equals(SorpresaDesfavorable.class)) return "SorpresaDesfavorable";
		if (efecto.getClass().equals(SorpresaCambioDeVehiculo.class)) return "SorpresaCambioDeVehiculo";
		if (efecto.getClass().equals(Pozo.class)) return "Pozo";
		if (efecto.getClass().equals(Piquete.class)) return "Piquete";
		if (efecto.getClass().equals(ControlPolicial.class)) return "ControlPolicial";
		
		return "";
	}
	private String stringAPartirDeDificultad(Dificultad dif) {
		if (dif == null) return "";
		if (dif.getClass().equals(DificultadFacil.class))return "Facil";
		if (dif.getClass().equals(DificultadModerada.class)) return "Moderada";
		if (dif.getClass().equals(DificultadDificil.class)) return "Dificil";
		return "";
	}
}
